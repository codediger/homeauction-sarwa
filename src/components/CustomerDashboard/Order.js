import React, { Component } from "react";
import { Segment, Form, Header } from "semantic-ui-react";
// import { Link } from 'react-router-dom'
/* utils */
import crud from "../../utils/CRUDService";
/* config */
import { BASE_URL } from "../../config/host";
/* urls */
const userOrdersURL = `${BASE_URL}user/orders`;
const token = localStorage.getItem("token");

class Order extends Component {
  state = {
    orders: [],
    activeIndex: 0,
    hasLoaded: false
  };
  componentDidMount() {
    this.getOrders();
  }

  getOrders = async () => {
    const orders = await crud.getWithToken(userOrdersURL, token);
    this.setState({ orders });
  };

  handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;

    this.setState({ activeIndex: newIndex });
  };

  handleCalculateTotal = (price, quantity) => {
    const total = price * quantity;
    return total.toLocaleString("en");
  };

  render() {
    // const { orders } = this.state
    // const quantity = 8
    // const price = 8500
    return (
      <div>
        <Form>
          {/* {
            <Dimmer active={hasLoaded ? false : true}>
              <Loader content='Loading your auctions' />
            </Dimmer>
          } */}
          <Header as="h4" attached="top">
            Orders
          </Header>
          <Segment attached>
            <p>You haven't made any order</p>
            {/* <Accordion styled fluid>
              <Accordion.Title active={activeIndex === 0} index={0} onClick={this.handleClick} className="relative">
                <Icon name='dropdown' />
                12-09-12
                <Label attached='top right'>Total: $70.00</Label>
              </Accordion.Title>
              <Accordion.Content active={activeIndex === 0}>
                <div className="cd-order-items">
                  <div className="cd-order-item">
                    <div className="cart-item">
                      <Link to={`/product/${item.product_id}`}>
                        <div className="cart-item-image">
                          <img src={item.product_image_url} alt="product" />
                        </div>
                      </Link>
                      <div className="cart-details">
                        <div className="cart-details-meta">
                          <Link to={`/product/${item.product_id}`}>
                            <h3 className="cart-item-title">{item.product_name}</h3>
                          </Link>
                          <h4>Unit Price: <span>{price.toLocaleString('en')}</span></h4>
                          <h4>Quantity: <span>{quantity}</span></h4>
                        </div>
                      </div>
                      <div className="cart-price">
                        <span>&#8358;</span>
                        {this.handleCalculateTotal(price, quantity)}
                      </div>
                    </div>
                  </div>
                  <div className="cd-order-item">
                    <div className="cart-item">
                      <Link to={`/product/${item.product_id}`}>
                        <div className="cart-item-image">
                          <img src={item.product_image_url} alt="product" />
                        </div>
                      </Link>
                      <div className="cart-details">
                        <div className="cart-details-meta">
                          <Link to={`/product/${item.product_id}`}>
                            <h3 className="cart-item-title">{item.product_name}</h3>
                          </Link>
                          <h4>Unit Price: <span>{price.toLocaleString('en')}</span></h4>
                          <h4>Quantity: <span>{quantity}</span></h4>
                        </div>
                      </div>
                      <div className="cart-price">
                        <span>&#8358;</span>
                        {this.handleCalculateTotal(item.product_unit_price, quantity)}
                      </div>
                    </div>
                  </div>
                </div>
              </Accordion.Content>
            </Accordion> */}
          </Segment>
        </Form>
      </div>
    );
  }
}

export default Order;
