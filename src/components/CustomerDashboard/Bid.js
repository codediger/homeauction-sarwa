import React, { Component } from "react";
import {
  Segment,
  Form,
  Header,
  Label,
  Dimmer,
} from "semantic-ui-react";
import { Link } from "react-router-dom";
/* utils */
import crud from "../../utils/CRUDService";
import Loading from "../Single/Loader";
/* config */

import { BASE_URL } from "../../config/host";
/* urls */
const userBidsURL = `${BASE_URL}user/bids`;
const token = localStorage.getItem("token");

class Bid extends Component {
  state = {
    bids: [],
    bidStatus: "active",
    activeIndex: 0,
    hasLoaded: false
  };
  componentDidMount() {
    this.getBids();
  }

  getBids = async () => {
    const {
      data: {
        response: {
          data: { bids }
        }
      }
    } = await crud.getWithToken(userBidsURL, token);
    let b = [];
    for (let i in bids) {
      b.push(bids[i]);
    }
    this.setState({ bids: b, hasLoaded: true });
  };

  handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;

    this.setState({ activeIndex: newIndex });
  };

  render() {
    const { bids, hasLoaded } = this.state;
    return (
      <div>
        <Form>
          {
            <Dimmer active={hasLoaded ? false : true} inverted>
              <Loading />
            </Dimmer>
          }
          <Header as="h4" attached="top">
            My Bids
          </Header>
          <Segment attached>
            {hasLoaded && (
              <div className="cd-order-items">
                {bids.length > 0 ? (
                  bids.map((bid, index) => (
                    <div className="cd-order-item" key={`i-${index}`}>
                      <div className="cart-item relative">
                        <Link
                          to={`/auction/${
                            bid.auction_product_id.auction_product_id
                          }`}
                        >
                          <div className="cart-item-image">
                            <img
                              src={bid.auction_product_id.auction_image_url}
                              alt="product"
                            />
                          </div>
                        </Link>
                        <div className="cart-details">
                          <div className="cart-details-meta">
                            <Link
                              to={`/auction/${
                                bid.auction_product_id.auction_product_id
                              }`}
                            >
                              <h3 className="cart-item-title">
                                {bid.auction_product_id.auction_name}
                              </h3>
                            </Link>
                            <h4>
                              Top Bid Amount: <span>&#8358;</span>
                              <span>{bid.top_bid_amount}</span>
                            </h4>
                            <h4>
                              Your Bid Amount: <span>&#8358;</span>
                              <span>show user last bid</span>
                            </h4>
                          </div>
                        </div>
                        <div className="bid-status">
                          {bid.auction_product_id.auction_active === 1 ? (
                            <Label attached="top right" color="green">
                              Ongoing
                            </Label>
                          ) : (
                            <Label attached="top right" color="red">
                              Ended
                            </Label>
                          )}
                        </div>
                      </div>
                    </div>
                  ))
                ) : (
                  <p>You have no bids</p>
                )}
              </div>
            )}
          </Segment>
        </Form>
      </div>
    );
  }
}

export default Bid;
