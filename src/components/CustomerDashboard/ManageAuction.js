import React, { PureComponent } from "react";
import {
  Segment,
  Form,
  Header,
  Menu,
  Input,
  Icon,
  Dimmer,
  Table,
  Modal,
  Button,
  Item
} from "semantic-ui-react";
import Dropzone from "react-dropzone";
import { all, post } from "axios";
import SimpleMDE from "react-simplemde-editor";

/* import components and utils */
import Countdown from "../Single/CountdownTimer";
import Price from "../Single/Price";
import Loading from "../Single/Loader";

/* utils */
import crud from "../../utils/CRUDService";
import notify from "../../utils/notify";

/* config */
import { BASE_URL } from "../../config/host";

const token = localStorage.getItem("token");
const userAuctionsURL = `${BASE_URL}user/auctions`;

class ManageAuction extends PureComponent {
  state = {
    userAuctions: [],
    auctionName: "",
    auctionDetails: "",
    auctionStartingBid: "",
    auctionEndingDate: "",
    auctionImages: [],
    auctionTags: "",
    imagesURLs: [],
    auctionImagesNames: [],
    auctionId: "",
    updateAuctionName: "",
    updateAuctionDetails: "",
    updateAuctionStartingBid: "",
    updateAuctionEndingDate: "",
    updateAuctionImages: [],
    updateAuctionTags: "",
    activeItem: "read",
    searchParameter: "",
    filteredArray: [],
    isLoading: false,
    showSaveButton: false,
    disableFormInput: true,
    hasLoaded: false,
    searching: false,
    ongoingXHR: false,
    open: false
  };

  componentDidMount() {
    this.getItems();
  }

  async getItems() {
    const {
      data: {
        response: {
          data: { auctions }
        }
      }
    } = await crud.getWithToken(userAuctionsURL, token);
    let userAuctions = [];
    for (let i in auctions) {
      userAuctions.push(auctions[i]);
    }
    this.setState({ userAuctions, hasLoaded: true });
  }

  clearForm = () => {
    this.setState({
      auctionName: "",
      auctionDetails: "",
      auctionStartingBid: "",
      auctionTags: "",
      auctionEndingDate: "",
      auctionImagesNames: ""
    });
  };

  handleInputChange = e => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  open = () => this.setState({ open: true });

  close = () => this.setState({ open: false });

  onClickEdit = () => {
    this.setState({
      disableFormInput: false,
      showSaveButton: true
    });
  };

  post = (URL, action) => {
    this.setState({ ongoingXHR: true });
    const {
      auctionName,
      auctionDetails,
      auctionStartingBid,
      auctionEndingDate,
      auctionTags
    } = this.state;
    let imagesArray = [];
    // split auctionTags string to an array
    let tags = [];
    if (auctionTags !== undefined) {
      tags = auctionTags.split(" ");
    }

    // Push all the axios request promise into a single array
    const uploaders = this.state.auctionImages.map(file => {
      // Initial FormData
      const formData = new FormData();
      formData.append("file", file);
      formData.append("tags", tags);
      formData.append("upload_preset", "xaj0173i"); // Replace the preset name with your own
      formData.append("api_key", "998157811734252"); // Replace API key with your own Cloudinary key
      formData.append("timestamp", (Date.now() / 1000) | 0);
      // Make an AJAX upload request using Axios (replace Cloudinary URL below with your own)
      return post(
        "https://api.cloudinary.com/v1_1/orinami/image/upload",
        formData,
        {
          headers: { "X-Requested-With": "XMLHttpRequest" }
        }
      ).then(response => {
        const { data } = response;
        // push image to array for storage
        imagesArray = [...imagesArray, data.secure_url];
        this.setState(prevState => ({
          imagesURLs: [...prevState.imagesURLs, data.secure_url]
        }));
      });
    });

    // Start Uploading
    all(uploaders).then(response => {
      console.log("Images have all being uploaded");

      /*===============================
      HANDLE CREATE UPDATE
      =================================*/

      const data = {
        auction_name: auctionName,
        auction_details: auctionDetails,
        starting_bid: auctionStartingBid,
        auction_image_url: imagesArray[0],
        images: imagesArray.splice(1),
        auction_tags: tags,
        auction_ended_at: auctionEndingDate
      };
      const create = async (data, token) => {
        return await crud.postWithToken(URL, data, token).then(response => {
          if (response.status === 200) {
            // close dimmer
            this.setState({ ongoingXHR: false });
            // show the success notification
            notify("success", `Auction successfully ${action}d`);
            // close modal where applicable
            this.close();
            // fetch new user auctions
            this.getItems();
            //clear the form states
            this.clearForm();
          } else {
            this.setState({ ongoingXHR: false });
            notify(
              "error",
              `Something is wrong, I could not ${action} that auction. Try Again`
            );
          }
        });
      };
      create(data, token);
    });
  };

  filteredArray = value => {
    const { userAuctions } = this.state;
    const filteredArray = userAuctions.filter(
      i => i.auction_name.toLowerCase() === value.toLowerCase().trim()
    );
    this.setState({ filteredArray });
  };

  createAuction = e => {
    e.preventDefault();
    this.post(`${BASE_URL}auction/create`, "create");
  };

  updateAuction = e => {
    e.preventDefault();
    this.post(`${BASE_URL}auction/update/${this.state.auctionId}`, "update");
  };

  deleteAuction = async id => {
    await crud
      .getWithToken(`${BASE_URL}auction/delete/${id}`, token)
      .then(response => {
        if (response.status === 200) {
          // close dimmer
          this.setState({ ongoingXHR: false });
          // show the success notification
          notify("success", "Auction Successfully Deleted");
          // fetch new user auctions
          this.getItems();
          this.filteredArray("");
          this.clearForm();
        } else {
          notify(
            "error",
            "Something is wrong, I could not delete that auction. Try Again"
          );
        }
      });
    // const array = userAuctions.filter(i => i.auction_name.toLowerCase() === e.target.value.toLowerCase())
    // this.setState({ filteredArray: array })
  };

  // Hold the images in state until the user creates the auction
  handleOnDropImages = files => {
    let names = [];
    for (let i in files) {
      names.push(files[i].name + " ");
      // names.push(files[i].name + ' ')
    }
    this.setState({
      auctionImages: files,
      auctionImagesNames: names
    });
  };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  handleMDEChange = value => {
    this.setState({ auctionDetails: value });
  };

  handleAuctionSearch = (e, { value }) => {
    this.setState({ loading: true, searchParameter: value });
    if (e.target.value === "") {
      this.setState({ searching: false });
    } else {
      this.setState({ searching: true });
    }
    this.filteredArray(value);
  };

  handleOnKeyUp = () => {
    setTimeout(() => this.setState({ searching: false }), 1500);
  };

  passItemToModal = item => {
    this.open();
    this.setState({
      auctionId: item.auction_product_id,
      auctionName: item.auction_name,
      auctionDetails: item.auction_details,
      auctionStartingBid: item.starting_bid,
      auctionEndingDate: item.auction_ended_at,
      auctionTags: item.auction_tags
    });
  };

  render() {
    const {
      activeItem,
      hasLoaded,
      userAuctions,
      searching,
      ongoingXHR
    } = this.state;
    let dropzoneRef;
    let s1 = "none",
      s2 = "none",
      s3 = "none",
      s4 = "none";

    if (activeItem === "read") {
      s1 = "block";
    } else if (activeItem === "create") {
      s2 = "block";
    } else if (activeItem === "update") {
      s3 = "block";
    } else {
      s4 = "block";
    }

    const getMarkdownOptions = {
      autofocus: false,
      spellChecker: true,
      initialValue: this.state.auctionDetails,
      placeholder: "Write details about your auction here"
    };

    return (
      <div className="cd-manage">
        <Menu widths={4} pointing>
          <Menu.Item
            name="read"
            active={activeItem === "read"}
            onClick={this.handleItemClick}
            content={"Your Auctions"}
          />
          <Menu.Item
            name="create"
            active={activeItem === "create"}
            onClick={this.handleItemClick}
            content={"Create Auction"}
          />
          <Menu.Item
            name="update"
            active={activeItem === "update"}
            onClick={this.handleItemClick}
            content={"Update Auction"}
          />
          <Menu.Item
            name="delete"
            active={activeItem === "delete"}
            onClick={this.handleItemClick}
            content={"Delete Auction"}
          />
        </Menu>
        {/* read */}
        <Form style={{ display: `${s1}` }}>
          {
            <Dimmer active={hasLoaded ? false : true} inverted>
              <Loading />
            </Dimmer>
          }
          <Header as="h4" attached="top">
            Auctions
          </Header>
          <Segment attached>
            <Item.Group divided>
              {hasLoaded && userAuctions.length > 0 ? (
                userAuctions.map(auction => (
                  <Item key={auction.auction_product_id}>
                    <Item.Image size="small" src={auction.auction_image_url} />
                    <Item.Content>
                      <h4 className="no-border">{auction.auction_name}</h4>
                      <Item.Description>
                        Auction Amount: <Price price={auction.starting_bid} />
                      </Item.Description>
                      <Item.Description>
                        <Countdown endTime={auction.auction_ended_at} />
                      </Item.Description>
                      <Item.Extra>
                        {auction.status === "pending" ? (
                          <div>
                            <Icon color="red" name="clock" /> Pending
                          </div>
                        ) : (
                          <div>
                            <Icon color="green" name="check" /> Approved
                          </div>
                        )}
                      </Item.Extra>
                    </Item.Content>
                  </Item>
                ))
              ) : (
                <p>You don't have any auction</p>
              )}
            </Item.Group>
          </Segment>
        </Form>
        {/* create */}
        <Form onSubmit={this.createAuction} style={{ display: `${s2}` }}>
          {
            <Dimmer active={ongoingXHR ? true : false}>
              <Loading />
            </Dimmer>
          }
          <Header as="h4" attached="top">
            Create Auction
          </Header>
          <Segment attached>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                label="Auction Name"
                name="auctionName"
                type="text"
                onChange={this.handleInputChange}
                required
                autoFocus
              />
            </Form.Group>
            <Form.Group widths="equal">
              {/* <Form.Input
                fluid
                label='Auction Type'
                name="auctionType"
                type="text"
                onChange={this.handleInputChange}
              /> */}
              <Form.Input
                fluid
                label="Your Starting Bid Price"
                name="auctionStartingBid"
                type="number"
                min="0"
                onChange={this.handleInputChange}
              />
              <Form.Input
                fluid
                label="Auction Ending Date"
                name="auctionEndingDate"
                type="date"
                onChange={this.handleInputChange}
              />
            </Form.Group>
            <Form.Group widths="equal">
              <Dropzone
                onDrop={this.handleOnDropImages}
                multiple
                accept="image/*"
                ref={node => (dropzoneRef = node)}
                style={{ display: "none" }}
              />
              <Form.Input
                fluid
                icon="plus"
                label="Choose Images"
                name="auctionImages"
                type="button"
                onClick={() => dropzoneRef.open()}
                value={this.state.auctionImagesNames}
              />
              <Form.Input
                fluid
                label="Auction Tags"
                name="auctionTags"
                type="text"
                placeholder="Seperate tags with a space."
                onChange={this.handleInputChange}
              />
            </Form.Group>
            <Form.Group widths="equal">
              {/* <TextArea
                autoHeight
                placeholder='Write more about this auction here'
                onChange={this.handleInputChange}
                style={{ minHeight: 100 }}
              /> */}
              <SimpleMDE
                onChange={this.handleMDEChange}
                options={getMarkdownOptions}
                value={this.state.auctionDetails}
              />
            </Form.Group>
            <Form.Group>
              <button className="button theme-button">
                <i className="fa fa-plus" aria-hidden="true">
                  {" "}
                </i>Create Auction
              </button>
            </Form.Group>
          </Segment>
        </Form>
        {/* update */}
        <Form style={{ display: `${s3}` }}>
          <Header as="h4" attached="top">
            Update an Auction
            <Input
              style={{ fontSize: "unset" }}
              autoFocus
              loading={searching}
              icon="search"
              name="searchParameter"
              placeholder="Search using auction name..."
              onChange={this.handleAuctionSearch}
              onKeyUp={this.handleOnKeyUp}
            />
          </Header>
          <Segment attached>
            {this.state.filteredArray.length > 0 ? (
              <Table>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>Auction Name</Table.HeaderCell>
                    <Table.HeaderCell>Action</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {this.state.filteredArray.map((item, index) => (
                    <Table.Row key={`i-${index}`}>
                      <Table.Cell>{item.auction_name}</Table.Cell>
                      <Table.Cell>
                        <Button
                          primary
                          size="mini"
                          onClick={() => this.passItemToModal(item)}
                        >
                          <Icon name="edit" /> Update
                        </Button>
                      </Table.Cell>
                    </Table.Row>
                  ))}
                </Table.Body>
              </Table>
            ) : this.state.searchParameter === "" ? (
              <p> Search for the auction you want to update</p>
            ) : (
              <p> Can't find any match</p>
            )}
            <Modal
              as={"form"}
              open={this.state.open}
              onClose={this.close}
              onSubmit={this.updateAuction}
            >
              {
                <Dimmer active={ongoingXHR ? true : false} inverted>
                  <Loading />
                </Dimmer>
              }
              <Modal.Header>Update {this.state.auctionName}</Modal.Header>
              <Modal.Content>
                <Modal.Description>
                  <div className="ui form">
                    <Form.Group widths="equal">
                      <Form.Input
                        fluid
                        label="Auction Name"
                        name="auctionName"
                        type="text"
                        onChange={this.handleInputChange}
                        defaultValue={this.state.auctionName}
                        required
                      />
                    </Form.Group>
                    <Form.Group widths="equal">
                      <Form.Input
                        fluid
                        label="Your Starting Bid Price"
                        name="auctionStartingBid"
                        type="number"
                        required
                        min={0}
                        onChange={this.handleInputChange}
                        defaultValue={this.state.auctionStartingBid}
                      />
                      <Form.Input
                        fluid
                        label="Auction Ending Date"
                        name="auctionEndingDate"
                        type="date"
                        onChange={this.handleInputChange}
                      />
                    </Form.Group>
                    <Form.Group widths="equal">
                      <Dropzone
                        onDrop={this.handleOnDropImages}
                        multiple
                        accept="image/*"
                        ref={node => (dropzoneRef = node)}
                        style={{ display: "none" }}
                      />
                      <Form.Input
                        fluid
                        icon="plus"
                        label="Choose Images"
                        name="auctionImages"
                        type="button"
                        onClick={() => dropzoneRef.open()}
                      />
                      <Form.Input
                        fluid
                        label="Auction Tags"
                        name="auctionTags"
                        type="text"
                        placeholder="Seperate tags with a space."
                        onChange={this.handleInputChange}
                        defaultValue={this.state.auctionTags}
                      />
                    </Form.Group>
                    <Form.Group widths="equal">
                      <SimpleMDE
                        onChange={this.handleMDEChange}
                        options={getMarkdownOptions}
                        label="markdown"
                      />
                    </Form.Group>
                  </div>
                </Modal.Description>
              </Modal.Content>
              <Modal.Actions>
                <button className="button theme-button">
                  <i className="fa fa-edit" aria-hidden="true">
                    {" "}
                  </i>Update Auction
                </button>
              </Modal.Actions>
            </Modal>
          </Segment>
        </Form>
        {/* delete */}
        <Form style={{ display: `${s4}` }}>
          <Dimmer active={ongoingXHR ? true : false}>
            <Loading />
          </Dimmer>
          <Header as="h4" attached="top">
            Delete auction
            <Input
              style={{ fontSize: "unset" }}
              autoFocus
              loading={searching}
              name="searchParameter"
              icon="search"
              placeholder="Search using auction name..."
              onChange={this.handleAuctionSearch}
            />
          </Header>
          <Segment attached>
            {this.state.filteredArray.length > 0 ? (
              <Table>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>Auction Name</Table.HeaderCell>
                    <Table.HeaderCell>Action</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {this.state.filteredArray.map(i => (
                    <Table.Row key={i.auction_product_id}>
                      <Table.Cell>{i.auction_name}</Table.Cell>
                      <Table.Cell>
                        <button
                          className="button remove-button"
                          onClick={() =>
                            this.deleteAuction(i.auction_product_id)
                          }
                        >
                          <i className="fa fa-trash-o" aria-hidden="true">
                            {" "}
                          </i>Delete
                        </button>
                      </Table.Cell>
                    </Table.Row>
                  ))}
                </Table.Body>
              </Table>
            ) : this.state.searchParameter === "" ? (
              <p> Search for the auction you want to delete</p>
            ) : (
              <p> Can't find any match</p>
            )}
          </Segment>
        </Form>
      </div>
    );
  }
}

export default ManageAuction;
