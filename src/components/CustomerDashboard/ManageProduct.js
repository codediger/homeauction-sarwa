import React, { PureComponent } from "react";
import {
  Segment,
  Form,
  Header,
  Item,
  Menu,
  Input,
  Icon,
  Dimmer,
  Table,
  Modal,
  Button,
  Dropdown
} from "semantic-ui-react";
import Dropzone from "react-dropzone";
import { post, all } from "axios";
import SimpleMDE from "react-simplemde-editor";

/* import components and utils */
import Price from "../Single/Price";
import Loading from "../Single/Loader";

/* utils */
import crud from "../../utils/CRUDService";
import notify from "../../utils/notify";

/* config */
import { BASE_URL } from "../../config/host";

const token = localStorage.getItem("token");
const userProductsURL = `${BASE_URL}user/products`;
const categoriesURL = `${BASE_URL}categories/fetch`;

class ManageProduct extends PureComponent {
  state = {
    userProducts: [],
    productName: "",
    productDescription: "",
    productQuantityPerUnit: 0,
    productUnitPrice: 0,
    productImages: [],
    categories: [],
    productTags: "",
    imagesURLs: [],
    productImagesNames: [],
    productId: "",
    categoryNames: "",
    activeItem: "read",
    searchParameter: "",
    filteredArray: [],
    isLoading: false,
    showSaveButton: false,
    disableFormInput: true,
    hasLoaded: false,
    searching: false,
    ongoingXHR: false,
    open: false
  };

  componentDidMount() {
    this.getItems();
  }

  async getItems() {
    const {
      data: {
        response: {
          data: { products }
        }
      }
    } = await crud.getWithToken(userProductsURL, token);
    let userProducts = [];

    for (let i in products) {
      userProducts.push(products[i]);
    }

    const categories = await crud.get(categoriesURL);
    this.setState({ userProducts, categories, hasLoaded: true });
  }

  clearForm = () => {
    this.setState({
      productName: "",
      productDescription: "",
      productQuantityPerUnit: 0,
      productUnitPrice: 0,
      productImagesNames: ""
    });
  };

  handleInputChange = e => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  };

  handleCategoryChange = (e, { value }) =>
    this.setState({ categoryNames: value });

  handleAddition = (e, { value }) => {
    this.setState({
      options: [{ text: value, value }, ...this.state.options]
    });
  };

  open = () => this.setState({ open: true });

  close = () => this.setState({ open: false });

  post = (URL, action) => {
    this.setState({ ongoingXHR: true });
    const {
      productName,
      productDescription,
      productQuantityPerUnit,
      productUnitPrice,
      categories,
      productTags,
      categoryNames
    } = this.state;
    let imagesArray = [];
    // split productTags string to an array
    let tags = [];
    if (productTags !== undefined) {
      tags = productTags.split(" ");
    }

    // Push all the axios request promise into a single array
    const uploaders = this.state.productImages.map(file => {
      // Initial FormData
      const formData = new FormData();
      formData.append("file", file);
      formData.append("tags", tags);
      formData.append("upload_preset", "xaj0173i"); // Replace the preset name with your own
      formData.append("api_key", "998157811734252"); // Replace API key with your own Cloudinary key
      formData.append("timestamp", (Date.now() / 1000) | 0);
      // Make an AJAX upload request using Axios (replace Cloudinary URL below with your own)
      return post(
        "https://api.cloudinary.com/v1_1/orinami/image/upload",
        formData,
        {
          headers: { "X-Requested-With": "XMLHttpRequest" }
        }
      ).then(response => {
        const { data } = response;
        // push image to array for storage
        imagesArray = [...imagesArray, data.secure_url];
        this.setState(prevState => ({
          imagesURLs: [...prevState.imagesURLs, data.secure_url]
        }));
      });
    });

    // Start Uploading
    all(uploaders).then(response => {
      /*===============================
      HANDLE CREATE/UPDATE
      =================================*/
      const { category_id } = categories.filter(
        i => i.category_name === categoryNames
      );
      const data = {
        product_name: productName,
        product_description: productDescription,
        product_quantity_per_unit: productQuantityPerUnit,
        product_image_url: imagesArray[0],
        images: imagesArray.splice(1),
        product_tags: tags,
        category_id,
        product_unit_price: productUnitPrice
      };
      const f = async (data, token) => {
        return await crud.postWithToken(URL, data, token).then(response => {
          if (response.status === 200) {
            // close dimmer
            this.setState({ ongoingXHR: false });
            // show the success notification
            notify("success", `Product successfully ${action}d`);
            // close modal where applicable
            this.close();
            // fetch new user auctions
            this.getItems();
            //clear the form states
            this.clearForm();
          } else {
            this.setState({ ongoingXHR: false });
            notify(
              "error",
              `Something is wrong, I could not ${action} that auction. Try Again`
            );
          }
        });
      };
      f(data, token);
    });
  };

  filteredArray = value => {
    const { userProducts } = this.state;
    const filteredArray = userProducts.filter(
      i => i.product_name.toLowerCase() === value.toLowerCase().trim()
    );
    this.setState({ filteredArray });
  };

  createProduct = e => {
    e.preventDefault();
    this.post(`${BASE_URL}product/create`, "create");
  };

  updateProduct = e => {
    e.preventDefault();
    this.post(`${BASE_URL}product/update/${this.state.productId}`, "update");
  };

  deleteProduct = async id => {
    await crud
      .getWithToken(`${BASE_URL}product/delete/${id}`, token)
      .then(response => {
        if (response.status === 200) {
          // close dimmer
          this.setState({ ongoingXHR: false });
          // show the success notification
          notify("success", "Product Successfully Deleted");
          // fetch new user auctions
          this.getItems();
          this.filteredArray("");
          this.clearForm();
        } else {
          notify(
            "error",
            "Something is wrong, I could not delete that product. Try Again"
          );
        }
      });
  };

  // Hold the images in state until the user creates the auction
  handleOnDropImages = files => {
    let names = [];
    for (let i in files) {
      names.push(files[i].name + " ");
      // names.push(files[i].name + ' ')
    }
    this.setState({
      productImages: files,
      productImagesNames: names
    });
  };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  handleMDEChange = value => {
    this.setState({ productDescription: value });
  };

  handleProductSearch = (e, { value }) => {
    this.setState({ loading: true, searchParameter: value });

    if (e.target.value === "") {
      this.setState({ searching: false });
    } else {
      this.setState({ searching: true });
    }
    this.filteredArray(value);
  };

  handleOnKeyUp = () => {
    setTimeout(() => this.setState({ searching: false }), 1500);
  };

  passItemToModal = item => {
    this.open();
    this.setState({
      productId: item.product_id,
      productName: item.product_name,
      productDescription: item.product_description,
      productQuantityPerUnit: item.product_quantity_per_unit,
      productUnitPrice: item.product_unit_price,
      productTags: item.product_tags
    });
  };

  render() {
    const {
      activeItem,
      hasLoaded,
      userProducts,
      searching,
      ongoingXHR,
      categories,
      open
    } = this.state;
    let dropzoneRef;
    let s1 = "none",
      s2 = "none",
      s3 = "none",
      s4 = "none";

    if (activeItem === "read") {
      s1 = "block";
    } else if (activeItem === "create") {
      s2 = "block";
    } else if (activeItem === "update") {
      s3 = "block";
    } else {
      s4 = "block";
    }

    const getMarkdownOptions = {
      autofocus: false,
      spellChecker: true,
      initialValue: this.state.productDescription,
      placeholder: "Write details about your product here"
    };

    let options = [];
    if (hasLoaded) {
      categories.map(i =>
        options.push({
          key: i.category_id,
          text: i.category_name.toLowerCase(),
          value: i.category_name.toLowerCase()
        })
      );
    }

    return (
      <div className="cd-manage">
        <Menu widths={4} pointing>
          <Menu.Item
            name="read"
            active={activeItem === "read"}
            onClick={this.handleItemClick}
            content={"Your Products"}
          />
          <Menu.Item
            name="create"
            active={activeItem === "create"}
            onClick={this.handleItemClick}
            content={"Create Product"}
          />
          <Menu.Item
            name="update"
            active={activeItem === "update"}
            onClick={this.handleItemClick}
            content={"Update Product"}
          />
          <Menu.Item
            name="delete"
            active={activeItem === "delete"}
            onClick={this.handleItemClick}
            content={"Delete Product"}
          />
        </Menu>
        {/* read */}
        <Form style={{ display: `${s1}` }}>
          {
            <Dimmer active={hasLoaded ? false : true} inverted>
              <Loading />
            </Dimmer>
          }
          <Header as="h4" attached="top">
            Products
          </Header>
          <Segment attached>
            <Item.Group divided>
              {hasLoaded && userProducts.length > 0 ? (
                userProducts.map(product => (
                  <Item key={product.product_id}>
                    <Item.Image size="small" src={product.product_image_url} />
                    <Item.Content>
                      <h4 className="no-border">{product.product_name}</h4>
                      <Item.Description>
                        Amount: <Price price={product.product_unit_price} />
                      </Item.Description>
                      <Item.Description>
                        Quantity: {product.product_quantity_per_unit}
                      </Item.Description>
                      <Item.Extra>
                        {product.status === "pending" ? (
                          <div>
                            <Icon color="red" name="clock" /> Pending
                          </div>
                        ) : (
                          <div>
                            <Icon color="green" name="check" /> Approved
                          </div>
                        )}
                      </Item.Extra>
                    </Item.Content>
                  </Item>
                ))
              ) : (
                <p>You don't have any auction</p>
              )}
            </Item.Group>
          </Segment>
        </Form>
        {/* create */}
        <Form onSubmit={this.createProduct} style={{ display: `${s2}` }}>
          {
            <Dimmer active={ongoingXHR ? true : false}>
              <Loading />
            </Dimmer>
          }
          <Header as="h4" attached="top">
            Create Product
          </Header>
          <Segment attached>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                label="Product Name"
                name="productName"
                type="text"
                onChange={this.handleInputChange}
                required
                autoFocus
              />
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                label="Product Quantity Per Unit"
                name="productQuantityPerUnit"
                type="number"
                min={0}
                onChange={this.handleInputChange}
              />
              <Form.Input
                fluid
                label="Product Unit Price"
                name="productUnitPrice"
                type="number"
                min={0}
                onChange={this.handleInputChange}
              />
            </Form.Group>
            <Form.Group widths="equal">
              <Dropdown
                placeholder="Choose Category"
                fluid
                selection
                style={style.dropdown}
                options={options}
                value={this.state.categoryNames}
                onChange={this.handleCategoryChange}
              />
              {/* <Dropdown
                placeholder='Choose Category'
                fluid
                multiple
                search
                selection
                allowAdditions
                style={style.dropdown}
                options={options}
                value={this.state.categoryNames}
                onAddItem={this.handleAddition}
                onChange={this.handleCategoryChange}
              /> */}
            </Form.Group>
            <Form.Group widths="equal">
              <Dropzone
                onDrop={this.handleOnDropImages}
                multiple
                accept="image/*"
                ref={node => (dropzoneRef = node)}
                style={{ display: "none" }}
              />
              <Form.Input
                fluid
                icon="plus"
                label="Choose Images"
                name="productImages"
                type="button"
                onClick={() => dropzoneRef.open()}
                value={this.state.productImagesNames}
              />
              <Form.Input
                fluid
                label="Product Tags"
                name="productTags"
                type="text"
                placeholder="Seperate tags with a space."
                onChange={this.handleInputChange}
              />
            </Form.Group>
            <Form.Group widths="equal">
              <SimpleMDE
                onChange={this.handleMDEChange}
                options={getMarkdownOptions}
                value={this.state.productDescription}
              />
            </Form.Group>
            <Form.Group>
              <button className="button theme-button">
                <i className="fa fa-plus" aria-hidden="true">
                  {" "}
                </i>Create Product
              </button>
            </Form.Group>
          </Segment>
        </Form>
        {/* update */}
        <Form style={{ display: `${s3}` }}>
          <Header as="h4" attached="top">
            Update a Product
            <Input
              style={{ fontSize: "unset" }}
              autoFocus
              loading={searching}
              icon="search"
              name="searchParameter"
              placeholder="Search using auction name..."
              onChange={this.handleProductSearch}
              onKeyUp={this.handleOnKeyUp}
            />
          </Header>
          <Segment attached>
            {this.state.filteredArray.length > 0 ? (
              <Table>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>Product Name</Table.HeaderCell>
                    <Table.HeaderCell>Action</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {this.state.filteredArray.map(item => (
                    <Table.Row key={item.product_id}>
                      <Table.Cell>{item.product_name}</Table.Cell>
                      <Table.Cell>
                        <Button
                          primary
                          size="mini"
                          onClick={() => this.passItemToModal(item)}
                        >
                          <Icon name="edit" /> Update
                        </Button>
                      </Table.Cell>
                    </Table.Row>
                  ))}
                </Table.Body>
              </Table>
            ) : this.state.searchParameter === "" ? (
              <p> Search for the product you want to update</p>
            ) : (
              <p> Can't find any match</p>
            )}
            <Modal
              as={"form"}
              open={open}
              onClose={this.close}
              onSubmit={this.updateProduct}
            >
              {
                <Dimmer active={ongoingXHR ? true : false} inverted>
                  <Loading />
                </Dimmer>
              }
              <Modal.Header>Update {this.state.productName}</Modal.Header>
              <Modal.Content>
                <Modal.Description>
                  <div className="ui form">
                    <Form.Group widths="equal">
                      <Form.Input
                        fluid
                        label="Product Name"
                        name="productName"
                        type="text"
                        onChange={this.handleInputChange}
                        defaultValue={this.state.productName}
                        required
                      />
                    </Form.Group>
                    <Form.Group widths="equal">
                      <Form.Input
                        fluid
                        label="Product Quantity Per Unit"
                        name="productQuantityPerUnit"
                        type="number"
                        min={0}
                        required
                        onChange={this.handleInputChange}
                        defaultValue={this.state.productQuantityPerUnit}
                      />
                      <Form.Input
                        fluid
                        label="Product Unit Price"
                        name="productUnitPrice"
                        type="number"
                        min={0}
                        onChange={this.handleInputChange}
                        defaultValue={this.state.productUnitPrice}
                      />
                    </Form.Group>
                    <Form.Group widths="equal">
                      <Dropdown
                        placeholder="Choose Category"
                        fluid
                        selection
                        style={style.dropdown}
                        options={options}
                        value={this.state.categoryNames}
                        onChange={this.handleCategoryChange}
                      />
                    </Form.Group>
                    <Form.Group widths="equal">
                      <Dropzone
                        onDrop={this.handleOnDropImages}
                        multiple
                        accept="image/*"
                        ref={node => (dropzoneRef = node)}
                        style={{ display: "none" }}
                      />
                      <Form.Input
                        fluid
                        icon="plus"
                        label="Choose Images"
                        name="productImages"
                        type="button"
                        onClick={() => dropzoneRef.open()}
                      />
                      <Form.Input
                        fluid
                        label="Product Tags"
                        name="productTags"
                        type="text"
                        placeholder="Seperate tags with a space."
                        onChange={this.handleInputChange}
                        defaultValue={this.state.productTags}
                      />
                    </Form.Group>
                    <Form.Group widths="equal">
                      <SimpleMDE
                        onChange={this.handleMDEChange}
                        options={getMarkdownOptions}
                        label="markdown"
                      />
                    </Form.Group>
                  </div>
                </Modal.Description>
              </Modal.Content>
              <Modal.Actions>
                <button className="button theme-button">
                  <i className="fa fa-edit" aria-hidden="true">
                    {" "}
                  </i>Update Product
                </button>
              </Modal.Actions>
            </Modal>
          </Segment>
        </Form>
        {/* delete */}
        <Form style={{ display: `${s4}` }}>
          <Dimmer active={ongoingXHR ? true : false}>
            <Loading />
          </Dimmer>
          <Header as="h4" attached="top">
            Delete Product
            <Input
              style={{ fontSize: "unset" }}
              autoFocus
              loading={searching}
              name="searchParameter"
              icon="search"
              placeholder="Search using product name..."
              onChange={this.handleProductSearch}
            />
          </Header>
          <Segment attached>
            {this.state.filteredArray.length > 0 ? (
              <Table>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>Product Name</Table.HeaderCell>
                    <Table.HeaderCell>Action</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {this.state.filteredArray.map(i => (
                    <Table.Row key={i.product_id}>
                      <Table.Cell>{i.product_name}</Table.Cell>
                      <Table.Cell>
                        <button
                          className="button remove-button"
                          onClick={() => this.deleteProduct(i.product_id)}
                        >
                          <i className="fa fa-trash-o" aria-hidden="true">
                            {" "}
                          </i>Delete
                        </button>
                      </Table.Cell>
                    </Table.Row>
                  ))}
                </Table.Body>
              </Table>
            ) : this.state.searchParameter === "" ? (
              <p> Search for the product you want to delete</p>
            ) : (
              <p> Can't find any match</p>
            )}
          </Segment>
        </Form>
      </div>
    );
  }
}

const style = {
  dropdown: {
    margin: 0,
    height: 40
  }
};
export default ManageProduct;
