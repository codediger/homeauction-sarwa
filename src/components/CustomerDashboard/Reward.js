import React, { Component } from "react";
import { Form, Header, Segment, Table } from "semantic-ui-react";

class Reward extends Component {
  render() {
    return (
      <div>
        <div className="cd-manage">
          <Form>
            <Header as="h4" attached="top">
              Your Rewards
            </Header>
            <Segment attached>
              <Table color="green">
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>Date</Table.HeaderCell>
                    <Table.HeaderCell>Reward</Table.HeaderCell>
                    <Table.HeaderCell>Description</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>

                <Table.Body>
                  <Table.Row>
                    <Table.Cell>30-12-18</Table.Cell>
                    <Table.Cell>2 Black Fremmiums</Table.Cell>
                    <Table.Cell>For creating an account </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
            </Segment>
          </Form>
        </div>
      </div>
    );
  }
}

export default Reward;
