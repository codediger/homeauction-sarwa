import React, { Component } from "react";
import { Segment, Form, Header } from "semantic-ui-react";

class PaymentDetails extends Component {
  state = {
    cardNumber: ""
  };
  handleInputChange = e => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  render() {
    return (
      <div>
        <Form>
          <Header as="h4" attached="top">
            Payment Details
          </Header>
          <Segment attached>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                label="Card Number"
                name="cardNumber"
                type="text"
                value={this.state.cardNumber}
                onChange={this.handleInputChange}
                required
                autoFocus
              />
            </Form.Group>
          </Segment>
        </Form>
      </div>
    );
  }
}

export default PaymentDetails;
