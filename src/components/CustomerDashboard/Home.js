import React, { Component } from "react";
import { Statistic, Grid } from "semantic-ui-react";
import { Line } from "react-chartjs-2";

/* utils */
import crud from "../../utils/CRUDService";
/* config */
import { BASE_URL } from "../../config/host";
const chartData = {
  data: {
    labels: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ],
    datasets: [
      {
        data: [28, 4, 0, 46, 5, 6, 2, 0, 3, 0, 6, 7],
        label: "Orders",
        borderColor: "purple",
        backgroundColor: "purple",
        fill: false,
        borderWidth: 1
      },
      {
        data: [4, 3, 0, 7, 25, 5, 12, 5, 0, 7, 34, 6],
        label: "Bids",
        borderColor: "midnightblue",
        backgroundColor: "midnightblue",
        fill: false,
        borderWidth: 1
      },
      {
        data: [2, 2, 4, 7, 3, 2, 0, 6, 7, 0, 6, 4],
        label: "Fremmiums Awarded",
        borderColor: "green",
        backgroundColor: "green",
        fill: false,
        borderWidth: 1
      }
    ]
  },
  options: {
    responsive: true,
    title: {
      display: true,
      text: "Here is your One Year Stat",
      fontFamily: "Lato",
      fontSize: 16,
      fontStyle: 300,
      fontColor: "black"
    },
    tooltips: {
      mode: "index",
      intersect: false,
      titleFontFamily: "Lato",
      titleFontSize: 16,
      titleFontColor: "white",
      titleFontStyle: 300,
      bodyFontFamily: "Lato",
      bodyFontStyle: 300,
      bodyFontColor: "white"
    },
    legend: {
      labels: {
        boxWidth: 20,
        fontFamily: "Lato",
        fontStyle: 300,
        fontColor: "black"
      }
    },
    hover: {
      mode: "nearest",
      intersect: true
    },
    scales: {
      xAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
            labelString: "Month"
          },
          ticks: {
            fontWeight: 300
          }
        }
      ],
      yAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
            labelString: "Count"
          },
          ticks: {
            fontWeight: 300
          }
        }
      ]
    }
  }
};

const token = localStorage.getItem("token");
const tokensCountURL = `${BASE_URL}user/tokens/count`;
const bidsCountURL = `${BASE_URL}user/bids/count`;
const ordersCountURL = `${BASE_URL}user/orders/count`;
const freemiumsCountURL = `${BASE_URL}user/freemiums/count`;

class Home extends Component {
  state = {
    ordersCount: 0,
    bidsCount: 0,
    tokensCount: 0,
    fremmiumsCount: 0,
    hasLoaded: false,
    showChart: window.innerWidth >= 600 ? true : false
  };

  componentDidMount() {
    this.getCounts();
  }
  getCounts = async () => {
    const {
      data: {
        response: {
          data: { no_of_orders }
        }
      }
    } = await crud.getWithToken(ordersCountURL, token);
    const {
      data: {
        response: {
          data: { no_of_bids }
        }
      }
    } = await crud.getWithToken(bidsCountURL, token);
    const {
      data: {
        response: {
          data: { no_of_tokens }
        }
      }
    } = await crud.getWithToken(tokensCountURL, token);
    const {
      data: {
        response: {
          data: { no_of_freemiums }
        }
      }
    } = await crud.getWithToken(freemiumsCountURL, token);
    this.setState({
      bidsCount: no_of_bids,
      tokensCount: no_of_tokens,
      ordersCount: no_of_orders,
      fremmiumsCount: no_of_freemiums,
      hasLoaded: true
    });
  };
  render() {
    const {
      bidsCount,
      ordersCount,
      tokensCount,
      fremmiumsCount,
      showChart
    } = this.state;
    const stat = [
      {
        label: "Orders",
        value: ordersCount,
        icon: "cubes",
        color: "purple"
      },
      {
        label: "Bids",
        value: bidsCount,
        icon: "hand-paper-o",
        color: "midnightblue"
      },
      {
        label: "Tokens",
        value: tokensCount,
        icon: "circle",
        color: "gold"
      },
      {
        label: "Fremmiums",
        value: fremmiumsCount,
        icon: "trophy",
        color: "green"
      }
    ];

    return (
      <div>
        <div className="cd-home-statistics">
          <Statistic.Group widths="four">
            {stat.map(st => (
              <Statistic
                key={st.label}
                style={{ boxShadow: `2px 0 1px 0 ${st.color} inset` }}
              >
                <Grid>
                  <Grid.Row width={18}>
                    <Grid.Row width={6}>
                      <i
                        className={`fa fa-${st.icon}`}
                        style={{ color: st.color }}
                      />
                    </Grid.Row>
                    <Grid.Row className="stat-details" width={12}>
                      <Statistic.Value className="stat-value">
                        {st.value}
                      </Statistic.Value>
                      <Statistic.Label className="stat-label">
                        {st.label}
                      </Statistic.Label>
                    </Grid.Row>
                  </Grid.Row>
                </Grid>
              </Statistic>
            ))}
          </Statistic.Group>
        </div>
        <div className="cd-border">
          {showChart && (
            <div className="cd-home-chart">
              <Line
                data={chartData.data}
                options={chartData.options}
                width={400}
                height={160}
              />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default Home;
