import React, { Component } from "react";
import {
  Segment,
  Form,
  Header,
  Dropdown,
  Menu,
  Statistic,
  Image
} from "semantic-ui-react";
import PaystackButton from "react-paystack";
/* utils */
import crud from "../../utils/CRUDService";
import notify from "../../utils/notify";
/* config */

import { BASE_URL } from "../../config/host";
/* urls */
const buyTokenURL = `${BASE_URL}token/buy`;
const token = localStorage.getItem("token");
const user = JSON.parse(localStorage.getItem("customerDetails"));

class Wallet extends Component {
  state = {
    key: "pk_test_67b61beb976eecb81333e26cc3068974682535b5", //PAYSTACK PUBLIC KEY
    email: "xyz@example.com", // default customer email
    quantity: 1,
    amount: 10000,
    userTokens: [],
    tokenTypes: [],
    activeItem: "wallet",
    disableRechargeButton: true,
    hasLoaded: false
  };

  componentDidMount() {
    const { email } = user;
    this.setState({ email });
    this.getTokens();
  }

  getTokens = async () => {
    const tokenTypes = await crud.get(`${BASE_URL}token/categories`);
    const userTokens = await crud.getWithToken(
      `${BASE_URL}token/user/details`,
      token
    );
    let t = [];
    for (let i in userTokens.data) {
      t.push(userTokens.data[i]);
    }
    this.setState({ userTokens: t, tokenTypes, hasLoaded: true });
  };

  callback = async response => {
    const { amount, quantity, tokenTypes } = this.state;
    const { id } = tokenTypes.find(i => i.token_amount === amount);
    const data = {
      reference: response.reference,
      token_quantity: quantity,
      token_id: id
    };

    await crud.postWithToken(buyTokenURL, data, token).then(response => {
      if (response.status === 200) {
        notify("success", "Token Balance Credited");
        const fetch = async (BASE_URL, token) => {
          const userTokens = await crud.getWithToken(
            `${BASE_URL}token/user/details`,
            token
          );
          let t = [];
          for (let i in userTokens.data) {
            t.push(userTokens.data[i]);
          }
          this.setState({ userTokens: t });
        };
        fetch(BASE_URL, token);
      } else {
        notify("error", "Your balance was not credited");
      }
    });
  };

  close = () => {
    console.log("Payment closed");
  };

  getReference = () => {
    //you can put any unique reference implementation code here
    let text = "";
    let possible =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < 32; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  };

  handleInputChange = e => {
    const t = e.target;
    this.setState({
      [t.name]: t.value,
      disableRechargeButton: false
    });
  };

  handleTokenTypeChange = (e, { value }) => {
    this.setState({ amount: value });
  };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    const {
      hasLoaded,
      userTokens,
      tokenTypes,
      activeItem,
      email,
      key,
      quantity,
      amount,
      disableRechargeButton
    } = this.state;
    let options = [];
    if (hasLoaded) {
      tokenTypes.map((i, index) =>
        options.push({
          key: `i-${index}`,
          text: i.token_rank,
          value: i.token_amount,
          description: i.token_amount
        })
      );
    }
    let s1 = "none",
      s2 = "none",
      s3 = "none";
    if (activeItem === "wallet") {
      s1 = "block";
    } else if (activeItem === "purchase") {
      s2 = "block";
    } else {
      s3 = "block";
    }
    const payableAmount = quantity * amount * 100;
    return (
      <div className="cd-wallet">
        <Menu widths={3} pointing>
          <Menu.Item
            name="wallet"
            active={activeItem === "wallet"}
            onClick={this.handleItemClick}
            content={"Your wallet"}
          />
          <Menu.Item
            name="purchase"
            active={activeItem === "purchase"}
            onClick={this.handleItemClick}
            content={"Purchase Token"}
          />
          <Menu.Item
            name="convert"
            active={activeItem === "convert"}
            onClick={this.handleItemClick}
            content={"Convert Token"}
          />
        </Menu>
        <Form style={{ display: `${s1}` }}>
          <Header as="h4" attached="top">
            Your wallet
          </Header>
          <Segment attached>
            <Statistic.Group className="cd-wallet-details">
              <Statistic.Group size="small">
                <Statistic className="cd-wallet-balance">
                  <Statistic.Label>Your balance</Statistic.Label>
                  <Statistic.Value>&#8358;40,500,000</Statistic.Value>
                </Statistic>
              </Statistic.Group>
              <Statistic.Group className="cd-wallet-tokens" size="small">
                {hasLoaded &&
                  userTokens.map((i, index) => (
                    <Statistic key={`i-${index}`}>
                      <Statistic.Value style={{ display: "flex" }}>
                        <Image
                          src={i.token_id.token_image}
                          inline
                          circular
                          style={{ maxHeight: "2rem" }}
                        />
                        <span style={{ fontSize: "1.6rem" }}>{i.quantity}</span>
                      </Statistic.Value>
                      <Statistic.Label>{i.token_id.token_rank}</Statistic.Label>
                    </Statistic>
                  ))}
              </Statistic.Group>
            </Statistic.Group>
          </Segment>
        </Form>
        <Form style={{ display: `${s2}` }}>
          <Header as="h4" attached="top">
            Purchase Token
          </Header>
          <Segment attached>
            <Form.Group widths="equal">
              <Form.Input
                placeholder="quantity"
                name="quantity"
                type="number"
                onChange={this.handleInputChange}
                required
                min={1}
              />
              {hasLoaded && (
                <Dropdown
                  fluid
                  onChange={this.handleTokenTypeChange}
                  options={options}
                  placeholder="Choose token type"
                  selection
                  required
                />
              )}
            </Form.Group>
            <Form.Group widths="equal">
              <div className="purchase-token">
                <PaystackButton
                  text="Buy"
                  class="button theme-button"
                  callback={this.callback}
                  close={this.close}
                  reference={this.getReference()}
                  email={email}
                  amount={payableAmount}
                  paystackkey={key}
                  disabled={disableRechargeButton}
                />
              </div>
            </Form.Group>
          </Segment>
        </Form>
        <Form style={{ display: `${s3}` }}>
          <Header as="h4" attached="top">
            Convert token to cash
          </Header>
          <Segment attached>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                placeholder="How many tokens do you want to convert"
                name="amount"
                type="number"
                min={1}
                onChange={this.handleInputChange}
                required
              />
              <Dropdown
                fluid
                onChange={this.handleInputChange}
                options={options}
                placeholder="Choose token type"
                selection
                required
              />
            </Form.Group>
            <Form.Group widths="equal">
              <div className="purchase-token">
                <PaystackButton
                  text="Revert"
                  class="button theme-button"
                  callback={this.callback}
                  close={this.close}
                  reference={this.getReference()}
                  email={email}
                  amount={payableAmount}
                  paystackkey={key}
                  disabled={disableRechargeButton}
                />
              </div>
            </Form.Group>
          </Segment>
        </Form>
      </div>
    );
  }
}

export default Wallet;
