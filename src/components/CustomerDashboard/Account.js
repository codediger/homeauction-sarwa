import React, { Component } from "react";
import { Segment, Form, Header } from "semantic-ui-react";
/* data */
import states from "../../data/states.json";
import countries from "../../data/countries.json";
/* utils */
import crud from "../../utils/CRUDService";
import notify from "../../utils/notify";
/* config */
import { BASE_URL } from "../../config/host";

class Account extends Component {
  state = {
    emailAddress: "",
    username: "",
    firstName: "",
    lastName: "",
    addressLineOne: "",
    addressLineTwo: "",
    zipCode: "",
    city: "",
    state: "",
    country: "",
    phone: "",
    altPhone: "",
    isLoading: false,
    showSaveButton: false,
    disableFormInput: true
  };

  // componentDidMount() {
  //   this.getUserDetails()
  // }

  // getUserDetails = async () => {
  //   const token = localStorage.getItem('token')
  //   await crud.getWithToken(`${BASE_URL}user/details`, token).then(response => {
  //     const { data: { response: { data: user } } } = response
  //     this.setState({
  //       emailAddress: user.email,
  //       username: user.username,
  //       firstName: user.first_name,
  //       lastName: user.last_name,
  //       addressLineOne: user.address1,
  //       addressLineTwo: user.address2,
  //       zipCode: user.zipcode,
  //       city: user.city,
  //       state: user.state,
  //       country: user.country,
  //       phone: user.mobile_number,
  //       altPhone: user.alternate_mobile_number
  //     })
  //   })
  // }

  componentDidMount() {
    const customerDetails = JSON.parse(localStorage.getItem("customerDetails"));
    for (let i in customerDetails) {
      if (customerDetails[i] === null) {
        customerDetails[i] = "";
      }
    }
    const {
      email,
      username,
      first_name,
      last_name,
      address1,
      address2,
      zipcode,
      city,
      state,
      country,
      mobile_number,
      alternate_mobile_number
    } = customerDetails;
    customerDetails !== null &&
      this.setState({
        emailAddress: email,
        username: username,
        firstName: first_name,
        lastName: last_name,
        addressLineOne: address1,
        addressLineTwo: address2,
        zipCode: zipcode,
        city,
        state,
        country,
        phone: mobile_number,
        altPhone: alternate_mobile_number
      });
  }

  handleInputChange = e => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  onClickEdit = () => {
    this.setState({
      disableFormInput: false,
      showSaveButton: true
    });
  };

  saveAccountDetails = async e => {
    e.preventDefault();
    this.setState({ isLoading: true });
    // Get user input
    const {
      emailAddress,
      username,
      firstName,
      lastName,
      addressLineOne,
      addressLineTwo,
      zipCode,
      city,
      state,
      country,
      phone,
      altPhone
    } = this.state;
    // Store input in payload
    const data = {
      email: emailAddress,
      username: username,
      first_name: firstName,
      last_name: lastName,
      address1: addressLineOne,
      address2: addressLineTwo,
      zipcode: zipCode,
      city,
      state,
      country,
      mobile_number: phone,
      alternate_mobile_number: altPhone
    };
    const token = localStorage.getItem("token");
    // update
    await crud
      .postWithToken(`${BASE_URL}user/update/details`, data, token)
      .then(response => {
        if (response.status === 200) {
          // show the success notification
          notify("success", "Account details updated successfully");
          const {
            data: {
              response: { data: user }
            }
          } = response;
          this.setState({
            emailAddress: user.email,
            username: user.username,
            firstName: user.first_name,
            lastName: user.last_name,
            addressLineOne: user.address1,
            addressLineTwo: user.address2,
            zipCode: user.zipcode,
            city: user.city,
            state: user.state,
            country: user.country,
            phone: user.mobile_number,
            altPhone: user.alternate_mobile_number,
            disableFormInput: true,
            showSaveButton: false,
            isLoading: false
          });
        } else {
          notify("error", "Something is wrong, Try saving again");
        }
      });
  };

  render() {
    const { disableFormInput, showSaveButton, isLoading } = this.state;
    return (
      <div className="">
        <Form loading={isLoading}>
          <Header as="h4" attached="top">
            Account Details
          </Header>
          <Segment attached>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                label="Email address"
                name="emailAddress"
                type="email"
                value={this.state.emailAddress}
                onChange={this.handleInputChange}
                disabled={disableFormInput}
                required
                autoFocus
              />
              <Form.Input
                fluid
                label="Username"
                name="username"
                type="text"
                value={this.state.username}
                onChange={this.handleInputChange}
                disabled={disableFormInput}
                required
              />
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                label="First name"
                name="firstName"
                type="text"
                value={this.state.firstName}
                onChange={this.handleInputChange}
                disabled={disableFormInput}
              />
              <Form.Input
                fluid
                label="Last name"
                name="lastName"
                type="text"
                value={this.state.lastName}
                onChange={this.handleInputChange}
                disabled={disableFormInput}
              />
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                label="Address Line 1"
                name="addressLineOne"
                type="text"
                value={this.state.addressLineOne}
                onChange={this.handleInputChange}
                disabled={disableFormInput}
              />
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                label="Address Line 2"
                name="addressLineTwo"
                type="text"
                value={this.state.addressLineTwo}
                onChange={this.handleInputChange}
                disabled={disableFormInput}
              />
            </Form.Group>
            <Form.Group widths="equal">
              {/* <Form.Select fluid label='Country' options={options} placeholder='Gender' required/> */}
              <Form.Input
                fluid
                label="Zip Code"
                name="zipCode"
                type="number"
                value={this.state.zipCode}
                onChange={this.handleInputChange}
                disabled={disableFormInput}
              />
              <Form.Input
                fluid
                label="City"
                name="city"
                type="text"
                value={this.state.city}
                onChange={this.handleInputChange}
                disabled={disableFormInput}
              />

              <Form.Select
                fluid
                label="State"
                name="state"
                options={states}
                onChange={this.handleSelectChange}
                disabled={disableFormInput}
              />
              <Form.Select
                fluid
                search
                label="Country"
                name="country"
                options={countries}
                onChange={this.handleSelectChange}
                disabled={disableFormInput}
              />
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                label="Phone"
                name="phone"
                type="number"
                value={this.state.phone}
                onChange={this.handleInputChange}
                disabled={disableFormInput}
              />
              <Form.Input
                fluid
                label="Alt. Phone"
                name="altPhone"
                type="number"
                value={this.state.altPhone}
                onChange={this.handleInputChange}
                disabled={disableFormInput}
              />
            </Form.Group>
            <Form.Group>
              {!showSaveButton ? (
                <button
                  className="button edit-button"
                  onClick={this.onClickEdit}
                >
                  {" "}
                  <i className="fa fa-edit" /> Edit
                </button>
              ) : (
                <button
                  className="button theme-button"
                  onClick={this.saveAccountDetails}
                >
                  <i className="fa fa-save" /> Save
                </button>
              )}
            </Form.Group>
          </Segment>
        </Form>
      </div>
    );
  }
}

export default Account;
