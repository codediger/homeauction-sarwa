import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
/* components */
import SearchBar from '../../Single/SearchBar';
import Loading from '../../Single/Loader';
import ProductItemCard from '../../Products/Shared/ProductItemCard';
/* utils */
import crud from '../../../utils/CRUDService';
import scrollTop from '../../../utils/scroll';
/* config */
import { BASE_URL } from '../../../config/host';

//const num = Math.floor(Math.random() * 50)
const ww = window.outerWidth;
const cardWidth = 224;
const ic = Math.floor(ww / cardWidth);
const featuredProductURL = `${BASE_URL}product/fetch/limit/${ic}`;
const categoriesURL = `${BASE_URL}categories/fetch/limit/12`;

class Body extends Component {
  state = {
    featuredProduct: [],
    categories: [],
    categoryNames: [],
    searchInput: '',
    hasLoaded: false
  };

  componentDidMount() {
    this.getFeaturedItems();
  }

  async getFeaturedItems() {
    let names = [];
    const featuredProduct = await crud.get(featuredProductURL);
    await crud.get(categoriesURL).then(categories => {
      for (let i in categories) {
        names.push(categories[i].category_name);
      }
    });
    this.setState({
      featuredProduct,
      categoryNames: names,
      hasLoaded: true
    });
  }

  onScrollTop = () => scrollTop();

  onClickSearch = e => {
    e.preventDefault();
    const { history } = this.props;
    const { searchInput } = this.state;
    history.push(`search?q=${searchInput}`);
  };

  onSearchValueChange = e => {
    e.persist();
    this.setState({ searchInput: e.target.value });
  };

  render() {
    let { categoryNames, hasLoaded, featuredProduct } = this.state;
    const products = featuredProduct.map(product => (
      <div className="m-featured-item" key={product.product_id}>
        <Link to={`/product/${product.product_id}`}>
          <ProductItemCard {...product} />
        </Link>
      </div>
    ));
    return (
      <div>
        <div className="m-home-landing">
          <div className="hero m-padding">
            <h1>
              Whatever you have is what you'd <span className="text">Pay!</span>
            </h1>
            <form action="post">
              <SearchBar
                history={this.props.history}
                onSearchValueChange={this.onSearchValueChange}
                onClickSearch={this.onClickSearch}
              />
            </form>
          </div>
          <div className="m-featured">
            <h4 className="f-title">Featured</h4>
            {hasLoaded ? <div className="f f-sb">{products}</div> : <Loading />}

            <button className="more-featured-items">
              <Link to="/featured">
                <h4>View More</h4>
              </Link>
            </button>
          </div>
          <div className="m-categories-container">
            <h4 className="cat-title">Top Categories</h4>
            <div className="m-categories">
              {hasLoaded ? (
                categoryNames.map((name, index) => (
                  <div className="m-categories-item" key={`i-${index}`}>
                    <Link to={`search?category=${name.toLowerCase()}`}>
                      {name}
                    </Link>
                  </div>
                ))
              ) : (
                <Loading />
              )}
            </div>
          </div>
          <div className="search-again-container">
            <button className="button theme-button" onClick={this.onScrollTop}>
              Search
            </button>
          </div>
        </div>
        <div className="bg-img">
          <span className="bg-overlay" />
          <img
            src="http://res.cloudinary.com/orinami/image/upload/f_auto,c_scale,q_25,w_1280/v1522420312/slava-keyzman-388932-unsplash_1_kunpzj.jpg"
            alt="homes.png"
          />
        </div>
      </div>
    );
  }
}

export default Body;
