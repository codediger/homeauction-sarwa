import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import auth from "../../../utils/AuthService";
import Capitalize from "../../../utils/Capitalize";

/* components */
import Logo from "../../Single/Logo";

class Header extends PureComponent {
  state = {
    cartCount: parseInt(localStorage.getItem("cartCount"), 10) || 0,
    isAuthenticated: auth.isAuthenticated(),
    username: localStorage.getItem("username")
  };

  componentDidMount() {
    this._intervalToGetCartCount();
  }

  componentWillMount() {
    clearInterval(this._intervalToGetCartCount);
  }

  _intervalToGetCartCount = () => {
    setInterval(() => {
      let c = parseInt(localStorage.getItem("cartCount"), 10);
      if (isNaN(parseInt(localStorage.getItem("cartCount"), 10))) {
        c = 0;
      }
      this.setState({ cartCount: c });
    }, 1000);
  };

  render() {
    const { isAuthenticated, username } = this.state;
    return (
      <div className="w-site-header">
        <div className="w-site-logo">
          <Link to="/">
            <Logo theme="black" />
          </Link>
        </div>
        <div className="w-site-items">
          <ul>
            <li className="w-site-item">
              <Link to="/cart">
                <i
                  className="fa fa-shopping-bag fa-2x"
                  aria-hidden="true"
                />
              </Link>
              <span className="cart-count">{this.state.cartCount}</span>
            </li>
            {!isAuthenticated && (
              <li className="w-site-item">
                <Link to="/signup">Signup</Link>
              </li>
            )}
            {!isAuthenticated && (
              <li className="w-site-item">
                <Link to="/login">Login</Link>
              </li>
            )}
            {isAuthenticated && (
              <li className="w-site-item">
                <Link to="/dashboard">
                  {`Logged in as ${Capitalize(username)}`}
                </Link>
              </li>
            )}
            {isAuthenticated && (
              <li className="w-site-item" style={{ color: "red" }}>
                <Link to="/logout">Logout</Link>
              </li>
            )}
          </ul>
        </div>
      </div>
    );
  }
}

export default Header;
