import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
/* components */
import Loading from "./../../Single/Loader";
import SearchBar from "../../Single/SearchBar";
import ProductItemCard from "../../Products/Shared/ProductItemCard";
/* utils */
import scrollTop from "../../../utils/scroll";

import {
  fetchProductsIfNeeded,
  fetchCategoriesIfNeeded
} from "../../../actions/index";

const windowWidth = window.outerWidth;
const cardWidth = 224;
const numberOfProductsToFetch = Math.floor((windowWidth / cardWidth) * 2) - 2;

function mapStateToProps({ products, categories }) {
  return {
    products,
    categories
  };
}

const mapDispatchToProps = {
  fetchProductsIfNeeded,
  fetchCategoriesIfNeeded
};

class Body extends Component {
  state = {
    searchInput: ""
  };

  componentDidMount() {
    const { fetchProductsIfNeeded, fetchCategoriesIfNeeded } = this.props;
    fetchProductsIfNeeded(numberOfProductsToFetch);
    fetchCategoriesIfNeeded(10);
  }

  onScrollTop = () => scrollTop();

  onClickSearch = e => {
    e.preventDefault();
    const { history } = this.props;
    const { searchInput } = this.state;
    history.push(`search?q=${searchInput}`);
  };

  onSearchValueChange = e => {
    e.persist();
    this.setState({
      searchInput: e.target.value
    });
  };

  render() {
    const { products, categories } = this.props;
    return (
      <Fragment>
        <div className="w-home-landing">
          <div className="hero w-padding">
            <h1>
              Whatever you have is what you 'd
              <span className="text">Pay!</span>
            </h1>
            <form action="post">
              <SearchBar
                history={this.props.history}
                onSearchValueChange={this.onSearchValueChange}
                onClickSearch={this.onClickSearch}
              />
            </form>
          </div>
          <div className="more-details-hero">
            <div className="top-products">
              <header>
                <h4> Top Products </h4>
                <span className="see-all">
                  <Link to="/search?type=product"> See all </Link>
                </span>
              </header>
              <div className="top-products-list">
                {products && products.isFetching ? (
                  <Loading />
                ) : (
                  products.items.map(product => (
                    <Link
                      to={`/product/${product.product_id}`}
                      key={product.product_id}
                      style={{ marginTop: 32 }}
                    >
                      <div className="top-products-list-item">
                        <ProductItemCard {...product} />
                      </div>
                    </Link>
                  ))
                )}
              </div>
            </div>
          </div>
          <div className="hot-deals-hero">
            <h3> Popular Categories </h3>
            <div className="ct-list">
              {categories && categories.isFetching ? (
                <Loading />
              ) : (
                categories.items.map((name, index) => (
                  <div className="ct-list-item" key={`i-${index}`}>
                    <Link
                      to={`search?category=${name.toLowerCase()}`}
                      className="ct-button"
                    >
                      {name}
                    </Link>
                  </div>
                ))
              )}
            </div>
          </div>
          <div className="product-display-hero">
            <img
              src="https://res.cloudinary.com/orinami/image/upload/c_fill,w_1280,h_160/f_auto/v1522425317/vadim-sherbakov-30-unsplash_akqmel.png"
              alt="products"
            />
            <div className="pd-links">
              <span className="pd-links-item">
                <Link to="/search?type=product"> Products </Link>
              </span>
            </div>
          </div>
        </div>
        <div className="bg-img">
          <span className="bg-overlay" />
          <img
            src="https://res.cloudinary.com/orinami/image/upload/f_auto,c_scale,q_25,w_1280/v1522420312/slava-keyzman-388932-unsplash_1_kunpzj.png"
            alt="landing.png"
          />
        </div>
      </Fragment>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Body);
