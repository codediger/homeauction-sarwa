import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import * as RouteHandler from "../config/RoutesHandler.js";

class Main extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={RouteHandler.Home} />
        <Route path="/product/:id" component={RouteHandler.Product} />
        <Route exact path="/cart" component={RouteHandler.Cart} />
        <Route exact path="/checkout" component={RouteHandler.Checkout} />
        <Route exact path="/search" component={RouteHandler.Search} />
        <Route exact path="/login" component={RouteHandler.Login} />
        <Route exact path="/signup" component={RouteHandler.Signup} />
        <Route
          exact
          path="/forgot-password"
          component={RouteHandler.ForgotPassword}
        />
        <Route
          exact
          path="/reset-password"
          component={RouteHandler.ResetPassword}
        />
        <Route exact path="/logout" component={RouteHandler.Logout} />
        {/* <Route
          exact
          path="/dashboard"
          component={RouteHandler.CustomerDashboard}
        />
        <Route
          path="/dashboard/:any"
          component={RouteHandler.CustomerDashboard}
        /> */}
        <Route exact component={RouteHandler.NotFound} />
      </Switch>
    );
  }
}

export default Main;
