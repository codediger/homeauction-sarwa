import React, { Component } from "react";
import { Link } from "react-router-dom";

import SearchBar from "../../Single/SearchBar";
import Logo from "../../Single/Logo";

class Header extends Component {
  state = {
    cartCount: parseInt(localStorage.getItem("cartCount"), 10) || 0,
    isAuthenticated: localStorage.getItem("isAuthenticated"),
    username: localStorage.getItem("username")
  };

  componentDidMount() {
    this._intervalToGetCartCount();

    const sidebar = document.getElementById("m-sidebar");
    const menu = document.getElementById("menu");

    menu.onclick = () => (sidebar.style.display = "flex");

    window.onclick = function(e) {
      if (e.target === sidebar) {
        sidebar.style.display = "none";
      }
    };
  }

  componentWillMount() {
    clearInterval(this._intervalToGetCartCount);
  }

  _intervalToGetCartCount = () => {
    setInterval(() => {
      let c = parseInt(localStorage.getItem("cartCount"), 10);
      if (isNaN(parseInt(localStorage.getItem("cartCount"), 10))) {
        c = 0;
      }
      this.setState({ cartCount: c });
    }, 1000);
  };

  render() {
    const { isAuthenticated, username } = this.state;
    return (
      <div>
        <header className="m-site-header-container">
          <div className="m-site-header">
            <Link to="/">
              <div className="site-logo">
                <Logo theme="white" />
              </div>
            </Link>
            <div className="site-items">
              <div className="site-menu">
                <Link to="/cart">
                  <div className="m-cart">
                    <i
                      className="fa fa-shopping-bag"
                      aria-hidden="true"
                      style={{ fontSize: "1.6em" }}
                    />
                    <span className="cart-count">{this.state.cartCount}</span>
                  </div>
                </Link>
              </div>
              <div className="site-menu" id="menu">
                <i className="fa fa-bars fa-2x" style={{ color: "white" }} />
              </div>
            </div>
          </div>
          <div className="m-site-header-sub">
            <form action="post">
              <SearchBar
                value={this.props.value}
                history={this.props.history}
                onSearchValueChange={this.props.onSearchValueChange}
                onClickSearch={this.props.onClickSearch}
                searchBarRef={this.props.searchBarRef}
              />
            </form>
          </div>
        </header>
        <div className="m-sidenav" id="m-sidebar">
          <div className="m-nav-items">
            <ul>
              <li className="m-nav-item">
                <Link to="/return-policy">Return Policy</Link>
              </li>
              <li className="m-nav-item">
                <Link to="/contact">
                  Call Us: <span>0700-600-0000 / 01-2778900</span>
                </Link>
              </li>
              <li className="m-nav-item">
                <Link to="/faq">FAQ</Link>
              </li>
              <li className="m-nav-item">
                <Link to="/search?type=auction">Auction on Alpalazzo</Link>
              </li>

              {!isAuthenticated && (
                <li className="m-nav-item">
                  <Link to="/login">Login</Link>
                </li>
              )}
              {!isAuthenticated && (
                <li className="m-nav-item">
                  <Link to="/signup">Signup</Link>
                </li>
              )}
              {isAuthenticated && (
                <li className="m-nav-item">
                  <Link to="/dashboard">Visit dashoboard</Link>
                </li>
              )}
              {isAuthenticated && (
                <li className="m-nav-item">
                  <Link to="/dashboard/account">
                    {username && `View Profile (${username})`}
                  </Link>
                </li>
              )}
              {isAuthenticated && (
                <li className="m-nav-item red-color">
                  <Link to="/logout" style={{ color: "red" }}>
                    Logout
                  </Link>
                </li>
              )}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
