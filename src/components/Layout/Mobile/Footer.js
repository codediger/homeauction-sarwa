import React, { PureComponent } from "react";
import { Link } from "react-router-dom";

class Footer extends PureComponent {
  render() {
    return (
      <div className="site-footer">
        <div className="site-footer-item">
          <Link to="/contact">Contact</Link>
        </div>
        <div className="site-footer-item">
          <Link to="/help">Help</Link>
        </div>
        <div className="site-footer-item">
          <Link to="/how-to">How To</Link>
        </div>
        <div className="site-footer-item">
          <Link to="/faq">Faq</Link>
        </div>
      </div>
    );
  }
}

export default Footer;
