import React, { Component } from "react";
import { Link } from "react-router-dom";
/* component */
import SearchBar from "../../Single/SearchBar";
import Logo from "../../Single/Logo";

class Header extends Component {
  state = {
    cartCount: parseInt(localStorage.getItem("cartCount"), 10) || 0,
    isAuthenticated: localStorage.getItem("isAuthenticated"),
    username: localStorage.getItem("username")
  };

  componentDidMount() {
    this._intervalToGetCartCount();
  }

  componentWillMount() {
    clearInterval(this._intervalToGetCartCount);
  }

  _intervalToGetCartCount = () => {
    setInterval(() => {
      let c = parseInt(localStorage.getItem("cartCount"), 10);
      if (isNaN(parseInt(localStorage.getItem("cartCount"), 10))) {
        c = 0;
      }
      this.setState({ cartCount: c });
    }, 1000);
  };

  render() {
    const { isAuthenticated, username } = this.state;
    return (
      <header className="oh-site-header">
        <div className="oh-super-nav">
          <ul>
            <li className="oh-super-nav-item">
              <Link to="/return-policy">Return Policy</Link>
            </li>
            <li className="oh-super-nav-item">
              <Link to="/contact">
                Call Us: <span>0700-600-0000 / 01-2778900</span>
              </Link>
            </li>
            <li className="oh-super-nav-item">
              <Link to="/faq">FAQ</Link>
            </li>
            <li className="oh-super-nav-item">
              <Link to="/search?type=auction">Auction on Alpalazzo</Link>
            </li>

            {!isAuthenticated && (
              <li className="oh-super-nav-item">
                <Link to="/login">Login</Link>
              </li>
            )}
            {!isAuthenticated && (
              <li className="oh-super-nav-item">
                <Link to="/signup">Signup</Link>
              </li>
            )}
            {isAuthenticated && (
              <li className="oh-super-nav-item">
                <Link to="/dashboard">Dashboard</Link>
              </li>
            )}
            {isAuthenticated && (
              <li className="oh-super-nav-item">
                <Link to="/dashboard/account">
                  {username && `Logged in as ${username}`}
                </Link>
              </li>
            )}
            {isAuthenticated && (
              <li className="oh-super-nav-item red-color">
                <Link to="/logout" style={{ color: "red" }}>
                  Logout
                </Link>
              </li>
            )}
          </ul>
        </div>
        <div className="oh-sub-nav">
          <div className="oh-site-logo">
            <Link to="/">
              <Logo theme="white" />
            </Link>
          </div>
          <div className="oh-search">
            <form>
              <SearchBar
                value={this.props.value}
                history={this.props.history}
                onSearchValueChange={this.props.onSearchValueChange}
                onClickSearch={this.props.onClickSearch}
                searchBarRef={this.props.searchBarRef}
              />
            </form>
          </div>
          <div className="oh-actions">
            {/* <Link to="/saved-items">
              <div className="oh-saved-items">
                <i className="fa fa-heart-o" aria-hidden="true"></i>
                <h4>Saved Items</h4>
              </div>
            </Link> */}
            <Link to="/cart">
              <div className="oh-cart">
                <i
                  className="fa fa-shopping-bag"
                  aria-hidden="true"
                  style={{ fontSize: "1.6em" }}
                />
                <span className="cart-count">{this.state.cartCount}</span>
              </div>
            </Link>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
