import React, { PureComponent } from "react";
import { Link } from "react-router-dom";

class Footer extends PureComponent {
  render() {
    return (
      <div className="w-site-footer">
        <div className="w-site-footer-column">
          <h4>Let us help you</h4>
          <span className="w-footer-item">Help center</span>
          <span className="w-footer-item">Contact Us</span>
          <span className="w-footer-item">FAQ</span>
          <span className="w-footer-item">Return Policy</span>
          <span className="w-footer-item">Become a Merchant</span>
        </div>

        <div className="w-site-footer-column">
          <h4>Get to Know Us</h4>
          <span className="w-footer-item">Careers</span>
          <span className="w-footer-item">About Us</span>
          <span className="w-footer-item">Terms and Conditions</span>
        </div>

        <div className="w-site-footer-column">
          <h4>How To</h4>
          <span className="w-footer-item">How to shop on Alpalazzo</span>
          <span className="w-footer-item">How to Auction on Alpalazzo</span>
          <span className="w-footer-item">How to bid on Alpalazzo</span>
        </div>

        <div className="w-site-footer-column">
          <h4>Customer Care Line</h4>
          <span className="w-footer-item">0700-600-0000 / 01-2778900</span>
          <div className="w-footer-item">
            <div className="social-icons">
              <Link to="">
                <i
                  className="fa fa-facebook-official fa-2x"
                  aria-hidden="true"
                />
              </Link>
              <Link to="">
                <i className="fa fa-twitter fa-2x" aria-hidden="true" />
              </Link>
              <Link to="">
                <i className="fa fa-instagram fa-2x" aria-hidden="true" />
              </Link>
              <Link to="">
                <i className="fa fa-skype fa-2x" aria-hidden="true" />
              </Link>
              <Link to="">
                <i className="fa fa-whatsapp fa-2x" aria-hidden="true" />
              </Link>
            </div>
          </div>
        </div>
        <div className="w-site-footer-column">
          <h4>Payment Methods</h4>
          <span className="w-footer-item">
            <img
              src="https://res.cloudinary.com/orinami/raw/upload/f_auto/v1517053691/cards_1_ejqaey.png"
              alt="paystack"
            />
          </span>
        </div>
      </div>
    );
  }
}

export default Footer;
