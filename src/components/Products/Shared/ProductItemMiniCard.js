import React, { PureComponent } from "react";
import MiniCard from "../../Single/MiniCard";

class ProductItemMiniCard extends PureComponent {
  render() {
    const data = {
      img_url: this.props.product_image_url,
      name: this.props.product_name,
      price: this.props.product_unit_price,
      tagName: "Product",
      rating: this.props.product_ranking
    };
    return <MiniCard {...data} />;
  }
}

export default ProductItemMiniCard;
