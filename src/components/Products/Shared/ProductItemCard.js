import React from "react";
import Card from "../../Single/Card";

const ProductItemCard = props => {
  const data = {
    img_url: props.product_image_url,
    name: props.product_name,
    price: props.product_unit_price,
    tagName: "Product"
  };
  return (
    <div>
      <Card {...data} />
    </div>
  );
};

export default ProductItemCard;
