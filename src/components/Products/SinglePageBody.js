import React, { Component } from "react";
import { Link } from "react-router-dom";
import ProductItemCard from "./Shared/ProductItemCard";
import LargeCard from "../Single/LargeCard";
import Loading from "../Single/Loader";

class SinglePageBody extends Component {
  render() {
    const mProduct = {
      otherMetaType: "product",
      id: this.props.product_id,
      price: this.props.product_unit_price,
      description: this.props.product_description,
      name: this.props.product_name,
      mainImage: this.props.product_image_url,
      otherImages: this.props.otherImages,
      tagName: "Product"
    };

    /* Quantity appended to Product */
    const productDetails = {
      ...this.props.product,
      quantity: this.props.quantity
    };

    return (
      <div>
        <div className="p-product-item">
          <LargeCard
            {...mProduct}
            addToCart={this.props.addToCart}
            handleQuantityChange={this.props.handleQuantityChange}
            product={productDetails}
            addToCartButtonDisabled={this.props.addToCartButtonDisabled}
          />
        </div>
        <div className="p-featured-products">
          <div className="top-products">
            <header>
              <h4>Featured Products</h4>
              <span className="see-all">
                <Link to="/search?type=product">See all</Link>
              </span>
            </header>
            <div className="top-products-list">
              {this.props.featuredProducts !== [] ? (
                this.props.featuredProducts.map(product => (
                  <div
                    className="top-products-list-item"
                    key={product.product_id}
                  >
                    <Link
                      to={`/product/${product.product_id}`}
                      onClick={() =>
                        this.props.loadProductItemPage(product.product_id)
                      }
                    >
                      <ProductItemCard {...product} />
                    </Link>
                  </div>
                ))
              ) : (
                <Loading />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SinglePageBody;
