import React, { Component } from "react";

const Layout = (Mobile, Web) =>
  class LayoutHOC extends Component {
    constructor() {
      super();
      this.state = {
        width: window.innerWidth
      };
    }

    componentWillMount() {
      window.addEventListener("resize", this.handleWindowSizeChange);
    }

    componentWillUnmount() {
      window.removeEventListener("resize", this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
      this.setState({ width: window.innerWidth });
    };

    render() {
      const { width } = this.state;
      const isMobile = width <= 600;

      if (isMobile) {
        return <Mobile {...this.props} />;
      } else {
        return <Web {...this.props} />;
      }
    }
  };

export default Layout;
