import React from "react";
import Rating from "../Single/Ratings";
import Countdown from "../Single/CountdownTimer";

export default ({ tagName, endTime, rating }) => {
  if (tagName === "Auction") {
    return <Countdown endTime={endTime} />;
  } else if (tagName === "Product") {
    return <Rating starRating={rating} />;
  } else {
    return <div />;
  }
};
