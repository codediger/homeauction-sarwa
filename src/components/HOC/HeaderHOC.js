import MobileHeaderWithoutSearch from "../Home/Mobile/Header";
import WebHeaderWithoutSearch from "../Home/Web/Header";

//with searchbar
import MobileHeaderWithSearch from "../Layout/Mobile/Header";
import WebHeaderWithSearch from "../Layout/Web/Header";
// Layout HOC for checking the width of the page to know which header to display
import LayoutHOC from "./Layout";

export default isDefault => {
  if (isDefault) {
    return LayoutHOC(MobileHeaderWithoutSearch, WebHeaderWithoutSearch);
  } else {
    return LayoutHOC(MobileHeaderWithSearch, WebHeaderWithSearch);
  }
};
