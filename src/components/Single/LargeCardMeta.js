import React, { PureComponent } from "react";
import { Label } from "semantic-ui-react";
import PaystackButton from "react-paystack";
import md from "marked";

/* import components */
import Button from "./ThemeButton";
import CardMetaTypeHOC from "../HOC/CardMetaTypeHOC";
import Price from "../Single/Price";

class LargeCardMeta extends PureComponent {
  state = {
    key: "pk_test_67b61beb976eecb81333e26cc3068974682535b5", //PAYSTACK PUBLIC KEY
    email: "foobar@example.com", // customer email
    amount: 10000, //equals NGN100,
    isAuthenticated: localStorage.getItem("isAuthenticated")
  };

  componentDidMount() {
    // Get the modal
    let modal = document.getElementById("myModal");
    // Get the button that opens the modal
    let btn = document.getElementById("placeBidButton");
    if (btn) {
      // When the user clicks on the button, open the modal
      btn.onclick = () => (modal.style.display = "flex");
      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(e) {
        if (e.target === modal) {
          modal.style.display = "none";
        }
      };
    }
  }

  callback = response => {
    console.log(response); // card charged successfully, get reference here
  };

  close = () => {
    console.log("Payment closed");
  };

  getReference = () => {
    //you can put any unique reference implementation code here
    let text = "";
    let possible =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-.=";

    for (let i = 0; i < 32; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  };

  render() {
    const { isAuthenticated } = this.state;
    const { userTokens, hasLoaded } = this.props;
    let description;
    if (
      this.props.description !== null ||
      this.props.description !== undefined
    ) {
      description = "";
    } else {
      description = this.props.description;
    }
    //Using the CardMetaTypeHOC HOC, this will choose the type of component to display - could be ratings (product) , coutdown(ratings) or nothing (service)
    let type = CardMetaTypeHOC(this.props);
    if (this.props.otherMetaType === "product") {
      return (
        <div className="l-card-details">
          <header>{this.props.name}</header>
          <div className="l-card-other-meta">
            <div className="l-card-price-container">
              <Price price={this.props.price} size="large" />
              <span className="divider">|</span>
              {type}
              {/* <span className="divider">|</span>
              <div className="save-for-later">
                <Rating icon='heart' />
                <span>Save for later</span>
              </div> */}
            </div>
            <div className="l-quantity">
              <h4>quantity</h4>
              <input
                type="number"
                min="1"
                max="1000"
                defaultValue="1"
                onChange={this.props.handleQuantityChange}
              />
            </div>
            <div className="l-action">
              <Button
                name="Add to Cart"
                addToCart={this.props.addToCart}
                {...this.props.product.id}
                addToCartButtonDisabled={this.props.addToCartButtonDisabled}
              />
              <span className="l-action-message">
                * Products can be bought with freemiums
              </span>
            </div>
          </div>
          <div className="l-card-description">
            {/* <p>{convertToHTML(this.props.description)}</p> */}
            <p>{this.props.description}</p>
          </div>
        </div>
      );
    } else if (this.props.otherMetaType === "auction") {
      let recharge = false;
      return (
        <div className="l-card-details">
          <header>{this.props.name}</header>
          <div className="l-card-other-meta">
            <div className="l-card-price-container">
              <Price price={this.props.price} size="large" />
              <span className="divider">|</span>
              {type}
              {/* <span className="divider">|</span>
              <div className="save-for-later">
                <Rating icon='heart' />
                <span>Save for later</span>
              </div> */}
            </div>
            <span className="l-card-price-message">
              (Minimum of{" "}
              <Label circular style={{ background: "gold", color: "white" }}>
                {isNaN(this.props.noOfTokenRequired)
                  ? ""
                  : this.props.noOfTokenRequired}
              </Label>{" "}
              gold token(s) required)
            </span>
            <div className="l-no-of-bids">
              <h4>
                No of Bids Placed: <span>{this.props.noOfBids}</span>
              </h4>
            </div>
            <div className="l-action">
              <button className="theme-button" id="placeBidButton">
                <i className="fa fa-shopping-bag" aria-hidden="true" />
                <span>Place Bid</span>
              </button>
              <span className="l-action-message">
                * You can only place another bid after someone matched your bid
                or placed a higher bid
              </span>
            </div>
          </div>
          <div className="l-card-description">
            {hasLoaded && md(description)}
          </div>
          <div id="myModal" className="modal">
            <div className="modal-content">
              <div className="modal-header">
                <h2>
                  Place a bid on <span>{this.props.name}</span>
                </h2>
                {isAuthenticated && (
                  <div>
                    Your Tokens:
                    <span className="f f-start token-container">
                      {hasLoaded &&
                        userTokens.map((i, index) => (
                          <span
                            className="f f-start token-layout"
                            key={`i-${index}`}
                          >
                            <span>{i.quantity}</span>
                            <img
                              src={i.token_id.token_image}
                              alt={i.token_id.token_rank}
                            />
                          </span>
                        ))}
                    </span>
                  </div>
                )}
              </div>
              <div className="modal-body">
                <div>
                  <Price price={this.props.price} />
                  <span className="l-card-price-message">
                    (Minimum of{" "}
                    <Label
                      circular
                      style={{ background: "gold", color: "white" }}
                    >
                      {isNaN(this.props.noOfTokenRequired)
                        ? ""
                        : this.props.noOfTokenRequired}
                    </Label>{" "}
                    gold token(s) required)
                  </span>
                </div>
                <div className="modal-quantity">
                  <h4>Bid:</h4>
                  <input
                    type="number"
                    name="quantity"
                    min={this.props.price}
                    onChange={this.props.handleBidAmountChange}
                    autoFocus
                    required
                  />
                </div>
                <p>
                  No of bids:<span>{this.props.noOfBids}</span>
                </p>
                {isAuthenticated && (
                  <p>
                    You've placed {this.props.itemBidCount} bids placed on this
                    item
                  </p>
                )}
              </div>
              {/* <div className="modal-footer">
                {
                  this.props.rechargeTokenBalance &&
                  <PaystackButton
                    text="Recharge your Token Balance"
                    class="button theme-button-inverse"
                    callback={this.callback}
                    close={this.close}
                    reference={this.getReference()}
                    email={this.state.email}
                    amount={this.state.amount}
                    paystackkey={this.state.key}
                  />
                }
                <button className="button theme-button" onClick={() => this.props.makeBid(this.props.id, this.props.bidAmount)}>
                  <i className="fa fa-shopping-bag" aria-hidden="true"></i>
                  Place Bid
                </button>
              </div> */}
              <div className="modal-footer">
                {recharge && (
                  <PaystackButton
                    text="Recharge your Token Balance"
                    class="button theme-button-inverse"
                    callback={this.callback}
                    close={this.close}
                    reference={this.getReference()}
                    email={this.state.email}
                    amount={this.state.amount}
                    paystackkey={this.state.key}
                  />
                )}
                <button
                  className="button theme-button"
                  onClick={() =>
                    this.props.makeBid(this.props.id, this.props.bidAmount)
                  }
                >
                  <i className="fa fa-shopping-bag" aria-hidden="true" />
                  Place Bid
                </button>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return;
    }
  }
}

export default LargeCardMeta;
