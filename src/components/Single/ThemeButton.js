import React, { PureComponent } from "react";

class Button extends PureComponent {
  render() {
    return (
      <button
        className="theme-button"
        onClick={this.props.addToCart}
        disabled={this.props.addToCartButtonDisabled}
      >
        <i className="fa fa-shopping-bag" aria-hidden="true" />
        <span>{this.props.name}</span>
      </button>
    );
  }
}

export default Button;
