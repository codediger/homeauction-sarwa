import React, { PureComponent } from "react";
import LazyLoad from "react-lazyload";

class OtherImages extends PureComponent {
  render() {
    const { sliceOtherImages, onImageClick } = this.props;
    return (
      <div className="l-card-other-images">
        {sliceOtherImages.map((i, index) => (
          <div className="l-card-oi-item" key={index}>
            <LazyLoad height={200}>
              <img src={i} alt="product" onClick={onImageClick} />
            </LazyLoad>
          </div>
        ))}
      </div>
    );
  }
}

export default OtherImages;
