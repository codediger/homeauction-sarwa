import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { Icon, Pagination } from 'semantic-ui-react';
/* components */
import ProductItemCard from '../Products/Shared/ProductItemCard';

class SearchResult extends PureComponent {
  render() {
    const {
      result,
      resultMessage,
      resultLength,
      hasLoaded,
      handlePaginationChange,
      itemsPerPage,
      focusSearchBar
    } = this.props;
    return (
      <div className="all-products-main">
        <header className="pad-b-24">
          {hasLoaded && <h3>{resultMessage}</h3>}
        </header>
        <div>
          {hasLoaded && result.length > 0 ? (
            <div className="top-products-list f-sa">
              {result.map((item, index) => (
                <div className="top-products-list-item" key={index}>
                  <Link to={`/product/${item.product_id}`}>
                    <ProductItemCard {...item} />
                  </Link>
                </div>
              ))}
            </div>
          ) : (
            <div className="search-not-found">
              <p>
                We couldn't find what you were looking for.{' '}
                <span role="img" aria-label=":cry">
                  &#128546;
                </span>
              </p>
              <button className="button" onClick={focusSearchBar}>
                Search again
              </button>
            </div>
          )}
        </div>

        {hasLoaded && resultLength > 24 && (
          <div className="all-products-pagination">
            <Pagination
              defaultActivePage={1}
              ellipsisItem={{
                content: <Icon name="ellipsis horizontal" />,
                icon: true
              }}
              firstItem={{
                content: <Icon name="angle double left" />,
                icon: true
              }}
              lastItem={{
                content: <Icon name="angle double right" />,
                icon: true
              }}
              prevItem={{ content: <Icon name="angle left" />, icon: true }}
              nextItem={{ content: <Icon name="angle right" />, icon: true }}
              totalPages={resultLength / itemsPerPage}
              onPageChange={handlePaginationChange}
            />
          </div>
        )}
      </div>
    );
  }
}

export default SearchResult;
