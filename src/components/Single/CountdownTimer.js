import React, { PureComponent } from "react";
import dayjs from "dayjs";

export default class CountdownTimer extends PureComponent {
  state = {
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0,
    message: "* This, bid has ended"
  };

  componentDidMount() {
    this.timer = setInterval(() => {
      const { endTime } = this.props;
      const finishTime2 = dayjs(endTime);
      const currentTime2 = dayjs();
      const timeDiff = finishTime2.diff(currentTime2);

      if (timeDiff > 0) {
        const days = finishTime2.diff(currentTime2, "days");
        const h = finishTime2.diff(currentTime2, "hours");
        const hours = h - days * 24;
        const m = finishTime2.diff(currentTime2, "minutes");
        const minutes = m - h * 60;
        const s = finishTime2.diff(currentTime2, "seconds");
        const seconds = s - m * 60;
        this.setState({ days, hours, minutes, seconds });
      }
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    const { days, hours, minutes, seconds } = this.state;
    let content;
    days === 0 &&
    hours === 0 &&
    minutes === 0 &&
    seconds === 0 &&
    this.props.endTime !== undefined
      ? (content = "Expired")
      : (content = `${days}d ${hours}h ${minutes}m ${seconds}s`);
    return <div className="card-timer-container">{content}</div>;
  }
}
