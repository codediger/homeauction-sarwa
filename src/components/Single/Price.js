import React, { PureComponent } from "react";

class Price extends PureComponent {
  state = {
    price: this.props.price
  };
  componentDidMount() {
    const { price } = this.state;
    price !== undefined &&
      this.setState({ price: this.props.price.toLocaleString("en") });
  }
  componentWillReceiveProps(props) {
    props.price !== undefined &&
      this.setState({ price: props.price.toLocaleString("en") });
  }
  render() {
    return this.props.size === "large" ? (
      <span className="l-card-price">
        <span>&#8358;</span>
        {this.state.price}
      </span>
    ) : (
      <span className="card-price">
        <span>&#8358;</span>
        {this.state.price}
      </span>
    );
  }
}

export default Price;
