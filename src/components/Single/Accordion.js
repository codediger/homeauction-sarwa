import React, { PureComponent } from "react";
import { Accordion, Icon, Checkbox } from "semantic-ui-react";

/* import components and utils */
import crud from "../../utils/CRUDService";
/* config */
import { BASE_URL } from "../../config/host";

const categoriesURL = `${BASE_URL}categories/fetch/limit/12`;

class _Accordion extends PureComponent {
  state = {
    categoryNames: [],
    hasLoaded: false
  };

  componentDidMount() {
    this.getCategories();
  }

  async getCategories() {
    let names = [];
    await crud.get(categoriesURL).then(categories => {
      for (let i in categories) {
        names.push(categories[i].category_name);
      }
    });
    this.setState({ categoryNames: names, hasLoaded: true });
  }

  render() {
    const {
      activeIndex,
      handleAccordionClick,
      handleInputChange,
      handlePriceRangeChange,
      typeCheckbox,
      categoryCheckbox
    } = this.props;
    const { hasLoaded, categoryNames } = this.state;

    return (
      <div className="accordion">
        <div className="f">
          <Accordion styled>
            <Accordion.Title
              active={activeIndex === 1}
              index={1}
              onClick={handleAccordionClick}
            >
              <Icon name="dropdown" />
              Category
            </Accordion.Title>
            <Accordion.Content active={activeIndex === 1} className="pad-tb-16">
              {hasLoaded &&
                categoryNames.map((item, index) => (
                  <div className="acc-label" key={`i-${index}`}>
                    <Checkbox
                      label={item}
                      name="categoryCheckbox"
                      value={item}
                      checked={categoryCheckbox === item}
                      onChange={handleInputChange}
                    />
                  </div>
                ))}
            </Accordion.Content>

            <Accordion.Title
              active={activeIndex === 2}
              index={2}
              onClick={handleAccordionClick}
            >
              <Icon name="dropdown" />
              Price
            </Accordion.Title>
            <Accordion.Content
              active={activeIndex === 2}
              className="price-range pad-tb-16"
              style={activeIndex === 2 ? { display: "flex" } : {}}
            >
              <input
                className="custom-input"
                min={0}
                max={1000000000}
                name="minPrice"
                placeholder="Min Price"
                type="number"
                onChange={handlePriceRangeChange}
              />
              <Icon name="minus" size="large" />
              <input
                className="custom-input"
                min={0}
                max={1000000000}
                name="maxPrice"
                placeholder="Max Price"
                type="number"
                onChange={handlePriceRangeChange}
              />
            </Accordion.Content>
          </Accordion>
        </div>
      </div>
    );
  }
}

export default _Accordion;
