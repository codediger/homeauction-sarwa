import React from "react";

const Loader = () => <div className="loader loader--mixed" />;

export default Loader;