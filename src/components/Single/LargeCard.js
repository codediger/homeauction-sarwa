import React, { PureComponent } from "react";
import LazyLoad from "react-lazyload";
import LargeCardMeta from "../Single/LargeCardMeta";
import OtherImages from "../Single/OtherImages";
import Lightbox from "react-image-lightbox"; // Package for opening images for zooming and all that

class LargeCard extends PureComponent {
  state = {
    photoIndex: 0,
    isOpen: false
  };

  onImageClick = () => {
    this.setState({ isOpen: true });
  };

  render() {
    const otherMeta = <LargeCardMeta {...this.props} />;
    const { photoIndex, isOpen } = this.state;
    const { mainImage, otherImages } = this.props;
    const sliceOtherImages = [...otherImages.slice(0, 3)];
    const images = [mainImage, ...otherImages];

    return (
      <div className="l-card-container">
        <div className="l-card">
          <div className="l-card-images">
            <div className="l-card-main-image">
              <LazyLoad height={500}>
                <img
                  src={mainImage}
                  alt="product"
                  onClick={this.onImageClick}
                />
              </LazyLoad>
            </div>
            <OtherImages
              sliceOtherImages={sliceOtherImages}
              onImageClick={this.onImageClick}
            />
          </div>

          {otherMeta}
        </div>
        {isOpen && (
          <Lightbox
            mainSrc={images[photoIndex]}
            nextSrc={images[(photoIndex + 1) % images.length]}
            prevSrc={images[(photoIndex + images.length - 1) % images.length]}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images.length
              })
            }
          />
        )}
      </div>
    );
  }
}

export default LargeCard;
