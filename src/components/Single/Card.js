import React, { PureComponent } from "react";
import LazyLoad from "react-lazyload";
import CardMetaTypeHOC from "../HOC/CardMetaTypeHOC";
import Price from "../Single/Price";

class Card extends PureComponent {
  render() {
    //Using the CardMetaTypeHOC HOC, this will choose the type of component to display - could be ratings (product) , coutdown(ratings) or nothing (service)
    const type = CardMetaTypeHOC(this.props);
    return (
      <div className="card">
        <LazyLoad height={220}>
          <img src={this.props.img_url} alt={this.props.tagName} />
        </LazyLoad>
        <div className="card-meta">
          <h4>{this.props.name}</h4>
          <div className="card-meta-details">
            <Price price={this.props.price} size="small" />
            <div className="card-meta-type">{type}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Card;
