import React from "react";
import { Rating } from "semantic-ui-react";

const Ratings = ({ rating }) => (
  <Rating icon="star" defaultRating={rating} maxRating={5} disabled />
);

Ratings.defaultProps = {
  rating: 0
};

export default Ratings;
