import React, { PureComponent } from "react";
import { Icon } from "semantic-ui-react";

class SearchBar extends PureComponent {
  handleOnKeyDown = () => {
    this.props.history.push('/search')
  }

  render() {
    const {
      onSearchValueChange,
      onClickSearch,
      value,
      searchBarRef
    } = this.props;
    return (
      <div className="search">
        <input
          type="text"
          name="search"
          id="search"
          placeholder="Search for anything..."
          required
          defaultValue={value || ""}
          onChange={onSearchValueChange}
          ref={searchBarRef}
          onKeyDown={this.handleOnKeyDown}
        />
        <button onClick={onClickSearch}>
          <Icon name="search" size="large" style={{ color: "#484848" }} />
        </button>
      </div>
    );
  }
}

export default SearchBar;
