import React from "react";
import LazyLoad from "react-lazyload";

const Logo = ({ theme }) => {
  const x =
    theme === "black" ? (
      <LazyLoad height={60}>
        <img
          src="https://res.cloudinary.com/orinami/image/upload/v1526415852/AlPalazzo_black_y7zvup_ugv3kc.svg"
          alt="logo"
        />
      </LazyLoad>
    ) : (
      <LazyLoad height={60}>
        <img
          src="https://res.cloudinary.com/orinami/image/upload/v1526415852/AlPalazzo_white_vblxjy.svg"
          alt="logo"
        />
      </LazyLoad>
    );
  return x;
};

export default Logo;
