import React, { PureComponent } from "react";
import CardMetaTypeHOC from "../HOC/CardMetaTypeHOC";
import Price from "../Single/Price";

class MiniCard extends PureComponent {
  render() {
    //Using the CardMetaTypeHOC HOC, this will choose the type of component to display - could be ratings (product) , coutdown(ratings) or nothing (service)
    const type = CardMetaTypeHOC(this.props);
    return (
      <div className="card">
        <div className="card-meta">
          {/* <img
            style={style.imageStyle}
            src={this.props.img_url}
            alt={this.props.tagName}
          /> */}
          <h4>{this.props.name}</h4>
          <div className="card-meta-details">
            <Price price={this.props.price} size="small" />
            <div className="card-meta-type mag-l-24">{type}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default MiniCard;
