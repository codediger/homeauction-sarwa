import MyLoadable from "./MyLoadable"; // customized config for code-splitting using react-loadable

export const Login = MyLoadable({
  loader: () => import("../pages/Login")
});

export const Signup = MyLoadable({
  loader: () => import("../pages/Signup")
});

export const Home = MyLoadable({
  loader: () => import("../pages/Home")
});

export const Product = MyLoadable({
  loader: () => import("../pages/Product")
});

export const Checkout = MyLoadable({
  loader: () => import("../pages/Checkout")
});

export const Cart = MyLoadable({
  loader: () => import("../pages/Cart")
});

export const NotFound = MyLoadable({
  loader: () => import("../pages/NotFound")
});

export const Logout = MyLoadable({
  loader: () => import("../pages/Logout")
});

export const ResetPassword = MyLoadable({
  loader: () => import("../pages/ResetPassword")
});

export const ForgotPassword = MyLoadable({
  loader: () => import("../pages/ForgotPassword")
});

export const Search = MyLoadable({
  loader: () => import("../pages/Search")
});

// export const CustomerDashboard = MyLoadable({
//   loader: () => import("../pages/CustomerDashboard")
// });

// export const DashboardHome = MyLoadable({
//   loader: () => import("../components/CustomerDashboard/Home")
// });

// export const Account = MyLoadable({
//   loader: () => import("../components/CustomerDashboard/Account")
// });

// export const Order = MyLoadable({
//   loader: () => import("../components/CustomerDashboard/Order")
// });

// export const ManageAuction = MyLoadable({
//   loader: () => import("../components/CustomerDashboard/ManageAuction")
// });

// export const Bid = MyLoadable({
//   loader: () => import("../components/CustomerDashboard/Bid")
// });

// export const PaymentDetails = MyLoadable({
//   loader: () => import("../components/CustomerDashboard/PaymentDetails")
// });

// export const Reward = MyLoadable({
//   loader: () => import("../components/CustomerDashboard/Reward")
// });
