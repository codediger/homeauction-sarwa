import { combineReducers } from "redux";
import {
  FETCH_PRODUCTS,
  FETCH_PRODUCTS_FAILURE,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_REQUEST,
  RECIEVE_PRODUCTS,
  FETCH_CATEGORIES,
  RECIEVE_CATEGORIES
} from "../constants/index";

function products(
  state = {
    isFetching: false,
    count: 0,
    items: []
  },
  action
) {
  switch (action.type) {
    case FETCH_PRODUCTS:
      return { ...state, isFetching: true };
    case RECIEVE_PRODUCTS:
      const items = [...state.items, ...action.items];
      return {
        ...state,
        isFetching: false,
        items,
        count: items.length,
        lastUpdatedAt: action.lastUpdatedAt
      };
    default:
      return state;
  }
}

function categories(
  state = {
    isFetching: false,
    items: [],
    count: 0
  },
  action
) {
  switch (action.type) {
    case FETCH_CATEGORIES:
      return { ...state, isFetching: true };
    case RECIEVE_CATEGORIES:
      const items = [...state.items, ...action.items];
      return {
        ...state,
        isFetching: false,
        items,
        count: items.length
      };
    default:
      return state;
  }
}

const rootReducers = combineReducers({
  products,
  categories
});

export default rootReducers;
