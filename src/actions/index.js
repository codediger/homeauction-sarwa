import { get, post } from "axios";
import { BASE_URL } from "../config/host";
import {
  FETCH_PRODUCTS,
  RECIEVE_PRODUCTS,
  FETCH_CATEGORIES,
  RECIEVE_CATEGORIES
} from "../constants/index";

function getProductURL(num) {
  return num !== undefined
    ? `${BASE_URL}product/fetch/limit/${num}`
    : `${BASE_URL}product/fetch`;
}

function getCategoriesURL(num) {
  return num !== undefined
    ? `${BASE_URL}categories/fetch/limit/${num}`
    : `${BASE_URL}categories/fetch`;
}

function fetchProducts() {
  return {
    type: FETCH_PRODUCTS
  };
}

function recieveProducts(items) {
  return {
    type: RECIEVE_PRODUCTS,
    items,
    count: items.length,
    lastUpdatedAt: Date.now()
  };
}

export function getProducts(num) {
  return function(dispatch) {
    dispatch(fetchProducts());

    const url = getProductURL(num);

    return get(url).then(response => dispatch(recieveProducts(response.data)));
  };
}

function shouldFetchProducts(state, num) {
  if (num > 0 && state.products.count < num) {
    return true;
  } else if (state.products.isFetching) {
    return false;
  } else {
    return null;
  }
}

export function fetchProductsIfNeeded(num) {
  return (dispatch, getState) => {
    if (shouldFetchProducts(getState(), num)) {
      dispatch(fetchProducts());
      dispatch(getProducts(num));
    }
  };
}

function fetchCategories() {
  return {
    type: FETCH_CATEGORIES
  };
}

function recieveCategories(items) {
  return {
    type: RECIEVE_CATEGORIES,
    items
  };
}
export function getCategories(num) {
  return function(dispatch) {
    dispatch(fetchCategories());

    const url = getCategoriesURL(num);

    return get(url).then(({ data }) => {
      const names = [];
      for (let i in data) {
        names.push(data[i].category_name);
      }
      dispatch(recieveCategories(names));
    });
  };
}

function shouldFetchCategories(state, num) {
  if (num > 0 && state.categories.count < num) {
    return true;
  } else if (state.categories.isFetching) {
    return false;
  } else {
    return null;
  }
}

export function fetchCategoriesIfNeeded(num) {
  return (dispatch, getState) => {
    if (shouldFetchCategories(getState(), num)) {
      dispatch(fetchCategories());
      dispatch(getCategories(num));
    }
  };
}