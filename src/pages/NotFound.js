import React, { Component } from "react";
import HeaderHOC from "../components/HOC/HeaderHOC";

const Header = HeaderHOC(false);

class NotFound extends Component {
  goBack = () => {
    window.history.back();
  };
  render() {
    return (
      <div>
        <Header />
        <div className="not-found">
          <h2>404</h2>
          <p>
            We couldn't find what you were looking for.{" "}
            <span role="img" aria-label=":cry">
              &#128546;
            </span>
          </p>
          <button className="button" onClick={this.goBack}>
            return to previous page
          </button>
        </div>
      </div>
    );
  }
}

export default NotFound;
