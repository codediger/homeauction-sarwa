import React, { Component } from "react";
import auth from "../utils/AuthService";

class Logout extends Component {
  componentWillMount() {
    const { history } = this.props;
    auth.logout(history);
  }
  render() {
    return <div />;
  }
}

export default Logout;
