import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment
} from "semantic-ui-react";
// import auth from '../utils/AuthService'
// import crud from '../utils/CRUDService'

const imgStyle = {
  height: "32px",
  width: "auto",
  marginBottom: "32px"
};

class ResetPassword extends Component {
  state = {
    password: "",
    confirmPassword: "",
    failedPasswordCheck: false,
    isAuthenticated: localStorage.getItem("isAuthenticated")
  };

  componentWillMount() {
    const { isAuthenticated } = this.state;
    const { history } = this.props;
    isAuthenticated && history.push("/");
  }

  handleInputChange = e => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  handleResetPassword = e => {
    e.preventDefault();
  };

  render() {
    const { isLoading } = this.state;
    return (
      <div className="login-form">
        <style>
          {`
        body > div,
        body > div > div,
        body > div > div > div.login-form {
          height: 100%;
        }
      `}
        </style>
        <Grid
          textAlign="center"
          style={{ height: "100%" }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Link to="/">
              <Header as="h1" color="grey" textAlign="center">
                <Image
                  src="http://res.cloudinary.com/orinami/image/upload/q_100/v1516976503/Logo_1_3_pdmwmx.png"
                  style={imgStyle}
                />
              </Header>
            </Link>
            <Form size="large" onSubmit={this.handleResetPassword}>
              <Segment stacked>
                <Header as="h4" color="grey" textAlign="center">
                  Enter your new Password
                </Header>
                {this.state.failedPasswordCheck && (
                  <Message negative>
                    <Message.Header>Error!</Message.Header>
                    <p>Your password doesn't match</p>
                  </Message>
                )}

                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  type="password"
                  name="password"
                  id="password"
                  value={this.state.password}
                  onChange={this.handleInputChange}
                  required
                />

                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Confirm Password"
                  type="password"
                  name="confirmPassword"
                  id="confirmPassword"
                  value={this.state.confirmPassword}
                  onChange={this.handleInputChange}
                  required
                />

                <Button fluid size="large" loading={isLoading} color="blue">
                  Reset
                </Button>
              </Segment>
            </Form>
            <Message>
              Have an account? <Link to="/login">Sign In</Link>
            </Message>
          </Grid.Column>
        </Grid>
        <div className="bg-img">
          <span className="bg-overlay-black" />
          <img
            src="https://res.cloudinary.com/orinami/image/upload/v1516973016/226861-P1MKZY-548_bp4vue.png"
            alt="homes.png"
          />
        </div>
      </div>
    );
  }
}

export default ResetPassword;
