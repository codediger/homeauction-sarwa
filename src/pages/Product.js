import React, { Component } from "react";
/* import components */
import Body from "../components/Products/SinglePageBody";
import MobileFooter from "../components/Layout/Mobile/Footer";
import WebFooter from "../components/Layout/Web/Footer";
/* utils */
import HeaderHOC from "../components/HOC/HeaderHOC";
import LayoutHOC from "../components/HOC/Layout";
import crud from "../utils/CRUDService";
import scrollTop from "../utils/scroll";
import notify from "../utils/notify";
/* config */
import { BASE_URL } from "../config/host";
/* HOCs */
const Header = HeaderHOC(false);
const Footer = LayoutHOC(MobileFooter, WebFooter);

const ww = window.outerWidth;
const cardWidth = 224;
let ic;
ww < 768
  ? (ic = Math.floor(ww / cardWidth))
  : (ic = Math.floor(ww / cardWidth) - 1);
const token = localStorage.getItem("token");
const featuredProductsURL = `${BASE_URL}product/fetch/limit/${ic}`;
const addToCartURL = `${BASE_URL}cart/add/item`;

class Product extends Component {
  state = {
    product: {},
    otherImages: [],
    featuredProducts: [],
    quantity: 1,
    addToCartButtonDisabled: true,
    isAuthenticated: localStorage.getItem("isAuthenticated")
  };

  componentDidMount() {
    this.getItems();
  }

  getItems = async () => {
    const {
      match: { params }
    } = this.props;
    let product = {};
    let otherImages = [];
    await crud.get(`${BASE_URL}product/fetch/${params.id}`).then(response => {
      product = { ...response };
      for (let i in response.images) {
        otherImages.push(response.images[i].product_image_url);
      }
    });
    const featuredProducts = await crud.get(featuredProductsURL);
    this.setState({
      product,
      otherImages,
      featuredProducts,
      addToCartButtonDisabled: false
    });
    scrollTop();
  };

  loadProductItemPage = async id => {
    const newProduct = await crud.get(`${BASE_URL}product/fetch/${id}`);
    this.setState({
      product: newProduct,
      quantity: 1
    });
    scrollTop();
  };

  handleQuantityChange = e => this.setState({ quantity: e.target.value });

  addToCart = async () => {
    const { history } = this.props;
    const {
      quantity,
      product: { product_id }
    } = this.state;
    if (!this.state.isAuthenticated) {
      history.push("/login");
      return;
    }
    //disable addToCartButton
    this.setState({ addToCartButtonDisabled: true });
    // Post item to cart database - to hold the individual cart for future storage
    const itemData = { product_id, quantity };
    await crud.postWithToken(addToCartURL, itemData, token).then(response => {
      if (response.status === 200) {
        // get old cartCount from localStorage and update it
        // find way to know if the product exists or not. if it does then don't update the count
        // else update the count. So I would need oj to send a status with his repsonse data
        /// { exist: 1 } so if exist is 1 don't count
        const count = parseInt(localStorage.getItem("cartCount"), 10);
        localStorage.setItem("cartCount", count + 1);
        ///
        notify("success", "Item added to cart");
        setTimeout(() => {
          this.setState({ addToCartButtonDisabled: false });
        }, 3000);
      } else {
        notify("error", "Could not add item to cart");
        setTimeout(() => {
          this.setState({ addToCartButtonDisabled: false });
        }, 3000);
      }
    });
  };

  render() {
    const { history } = this.props;
    return (
      <div>
        <Header history={history} />
        <Body
          handleQuantityChange={this.handleQuantityChange}
          addToCart={this.addToCart}
          quantity={this.state.quantity}
          {...this.state.product}
          otherImages={this.state.otherImages}
          featuredProducts={this.state.featuredProducts}
          loadProductItemPage={this.loadProductItemPage}
          addToCartButtonDisabled={this.state.addToCartButtonDisabled}
        />
        <Footer />
      </div>
    );
  }
}

export default Product;
