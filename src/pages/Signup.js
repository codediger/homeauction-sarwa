import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from "semantic-ui-react";
import auth from "../utils/AuthService";
import crud from "../utils/CRUDService";
import Logo from "../components/Single/Logo";

class Signup extends Component {
  state = {
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
    failedPasswordCheck: false,
    //isAuthenticated: false,
    isLoading: false,
    errorOccured: false,
    isAuthenticated: localStorage.getItem("isAuthenticated")
  };

  componentWillMount() {
    const { isAuthenticated } = this.state;
    const { history } = this.props;
    isAuthenticated && history.push("/");
  }

  handleInputChange = e => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  handleSignup = e => {
    e.preventDefault();
    // Check password match
    if (this.state.password !== this.state.confirmPassword) {
      this.setState({ failedPasswordCheck: true });
    } else {
      this.setState({ failedPasswordCheck: false });
    }
    // Get user input
    const { username, email, password, confirmPassword } = this.state;
    const { history } = this.props;
    // Store input in payload
    const data = {
      username: username,
      email: email,
      password: password,
      c_password: confirmPassword
    };
    this.setState({ isLoading: true });
    // Try Signing up the user
    try {
      const cb = async response => {
        if (response.status === 200) {
          // Retreive username and data
          const {
            user: { username },
            token
          } = response.data.response.data;
          // Store username and isAuthenticated
          localStorage.setItem("username", username);
          localStorage.setItem("isAuthenticated", true);
          // Store token in localstorage
          auth.storeToken(token);
          // Fetch the user details from the protected route using the token and store in localhost
          const customerDetails = await crud.getWithToken(
            "https://homeauction-sails.herokuapp.com/user/details",
            token
          );
          console.log(customerDetails);
          if (customerDetails.status === 200) {
            localStorage.setItem(
              "customerDetails",
              JSON.stringify(customerDetails.data)
            );
          }
          // Set state isAuthenticated to true
          this.setState({ isAuthenticated: true });
          // Send the user to home
          history.push("/");
        } else {
          this.setState({ isLoading: false, errorOccured: true });
          console.log("An error occured along the way. We would find out why");
        }
      };
      auth.signup(data, cb);
      setTimeout(
        () => this.setState({ isLoading: false, errorOccured: true }),
        5000
      );
    } catch (error) {
      this.setState({ isLoading: false });
    }
  };

  render() {
    const { isLoading } = this.state;
    return (
      <div className="login-form">
        {/*
        Heads up! The styles below are necessary for the correct render of this example.
        You can do same with CSS, the main idea is that all the elements up to the `Grid`
        below must have a height of 100%.
      */}
        <style>{`
        body > div,
        body > div > div,
        body > div > div > div.login-form {
          height: 100%;
        }
      `}</style>
        <Grid
          textAlign="center"
          style={{ height: "100%" }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 375 }}>
            <Link to="/">
              <Header
                as="h1"
                color="grey"
                textAlign="center"
                className="authpage-header"
              >
                <Logo theme="white" />
              </Header>
            </Link>
            <Form size="large" onSubmit={this.handleSignup}>
              <Segment stacked>
                <Header as="h4" color="grey" textAlign="center">
                  Create an account
                </Header>
                {this.state.failedPasswordCheck && (
                  <Message negative>
                    <Message.Header>Error!</Message.Header>
                    <p>Your password doesn't match</p>
                  </Message>
                )}
                {this.state.errorOccured && (
                  <Message
                    error
                    header="Something Went Wrong"
                    content="Sorry, It's probably my fault, please try again."
                  />
                )}

                <Form.Input
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder="Username"
                  name="username"
                  id="username"
                  type="text"
                  value={this.state.username}
                  onChange={this.handleInputChange}
                  required
                  autoFocus
                />
                <Form.Input
                  fluid
                  icon="mail"
                  iconPosition="left"
                  placeholder="E-mail address"
                  name="email"
                  id="email"
                  type="email"
                  value={this.state.email}
                  onChange={this.handleInputChange}
                  required
                />
                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  type="password"
                  name="password"
                  id="password"
                  value={this.state.password}
                  onChange={this.handleInputChange}
                  required
                />

                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Confirm Password"
                  type="password"
                  name="confirmPassword"
                  id="confirmPassword"
                  value={this.state.confirmPassword}
                  onChange={this.handleInputChange}
                  required
                />

                <Button
                  fluid
                  size="mini"
                  loading={isLoading}
                  className="theme-bgcolor-2"
                >
                  Create account
                </Button>
              </Segment>
            </Form>
            <Message>
              Have an account?{" "}
              <Link to="/login" style={{ color: "red" }}>
                {" "}
                Sign in
              </Link>
            </Message>
          </Grid.Column>
        </Grid>
        <div className="bg-img">
          <span className="bg-overlay" />
          <img
            src="https://res.cloudinary.com/orinami/image/upload/f_auto,c_scale,q_25,w_1280/q_auto/v1522627539/rawpixel-com-550994-unsplash_oiilxu.png"
            alt="homes.png"
          />
        </div>
      </div>
    );
  }
}

// Add Signup.prototype to your code for validation checks
export default Signup;
