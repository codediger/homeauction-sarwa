import React, { Component, Fragment } from "react";
/* components */
import MobileFooter from "../components/Layout/Mobile/Footer";
import WebFooter from "../components/Layout/Web/Footer";
import Accordion from "../components/Single/Accordion";
import SearchResult from "../components/Single/SearchResult";
import Loading from "../components/Single/Loader";
/* utils */
import crud from "../utils/CRUDService";
import scrollTop from "../utils/scroll";
import HeaderHOC from "../components/HOC/HeaderHOC";
import LayoutHOC from "../components/HOC/Layout";
/* config */
import { BASE_URL } from "../config/host";
/* HOCs */
const Header = HeaderHOC(false);
const Footer = LayoutHOC(MobileFooter, WebFooter);

const itemsPerPage = 24;
const searchURL = `${BASE_URL}search`;

class Search extends Component {
  constructor(props) {
    super(props);
    this.searchBarRef = null;
    this.setSearchBarRef = element => {
      this.searchBarRef = element;
    };

    this.focusSearchBar = () => {
      if (this.searchBarRef) this.searchBarRef.focus();
    };

    this.state = {
      width: window.innerWidth,
      result: [],
      resultMessage: "",
      resultArray: [],
      activeIndex: 0,
      activePage: 1,
      minPrice: 0,
      maxPrice: 0,
      searchParam: "",
      typeCheckbox: "",
      categoryCheckbox: "",
      hasLoaded: false,
      filterToggle: false
    };
  }

  componentDidMount() {
    window.addEventListener("resize", this.handleWindowSizeChange);
    this._getItems();
    this.focusSearchBar();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  async _search(search) {
    const r = await crud.get(`${searchURL}${search}`);
    const { response } = { ...r };
    const result = response.data.slice(0, 24);
    const resultMessage = response.message;
    const resultArray = response.data;
    this.setState({ result, resultArray, resultMessage, hasLoaded: true });
  }

  _getItems() {
    const {
      location: { search }
    } = this.props;
    this._search(search);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  handleAccordionClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;
    this.setState({ activeIndex: newIndex });
  };

  handleInputChange = (e, { name, value }) => {
    this.setState({ [name]: value });

    const { history } = this.props;
    const query = new URLSearchParams(history.location.search);

    name === "typeCheckbox" && query.set("type", value.toLowerCase());
    name === "categoryCheckbox" && query.set("category", value.toLowerCase());
    //replace url
    history.replace({ ...history.location, search: query.toString() });
    this.setState({ hasLoaded: false });
    this._search(`?${query.toString()}`);
  };

  handlePriceRangeChange = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value });

    const { history } = this.props;
    const query = new URLSearchParams(history.location.search);
    if (name === "minPrice") {
      value > 0
        ? query.set("minPrice", parseInt(value, 10))
        : query.delete("minPrice");
    }
    if (name === "maxPrice") {
      value > 0
        ? query.set("maxPrice", parseInt(value, 10))
        : query.delete("maxPrice");
    }
    //replace url
    history.replace({ ...history.location, search: query.toString() });
    this.setState({ hasLoaded: false });
    this._getItems();
  };

  handlePaginationChange = async (e, { activePage }) => {
    const { resultArray } = this.state;
    let startNumber;
    if (activePage === 1) {
      startNumber = activePage * 1;
    } else {
      startNumber = activePage * itemsPerPage - itemsPerPage;
    }

    const endNumber = startNumber + itemsPerPage;
    const result = resultArray.slice(startNumber, endNumber);
    this.setState({ activePage, result });
    scrollTop();
  };

  onClickSearch = async e => {
    e.preventDefault();

    this.setState({ hasLoaded: false });
    const { history } = this.props;
    const { searchInput } = this.state;
    const query = new URLSearchParams(history.location.search);
    query.set("q", searchInput);
    history.replace({ ...history.location, search: query.toString() });

    await crud.get(`${searchURL}?${query}`).then(response => {
      const {
        response: { data }
      } = response;
      this.setState({ result: data, hasLoaded: true });
    });
  };

  onSearchValueChange = e => {
    this.setState({ searchInput: e.target.value });
  };

  showFilters = () => {
    this.setState(prevState => ({ filterToggle: !prevState.filterToggle }));
  };

  render() {
    // activeIndex is for the Accordion, activePage is for the pagination
    const {
      width,
      activeIndex,
      minPrice,
      maxPrice,
      hasLoaded,
      result,
      resultMessage,
      resultArray,
      searchParam,
      typeCheckbox,
      categoryCheckbox,
      filterToggle
    } = this.state;
    const { history } = this.props;
    const chooseLayout = width < 900;
    const resultLength = resultArray.length;
    const accordion = (
      <Accordion
        activeIndex={activeIndex}
        minPrice={minPrice}
        maxPrice={maxPrice}
        typeCheckbox={typeCheckbox}
        categoryCheckbox={categoryCheckbox}
        handleInputChange={this.handleInputChange}
        handleAccordionClick={this.handleAccordionClick}
        handlePriceRangeChange={this.handlePriceRangeChange}
      />
    );

    return <Fragment>
        <Header history={history} value={searchParam} onSearchValueChange={this.onSearchValueChange} onClickSearch={this.onClickSearch} searchBarRef={this.setSearchBarRef} />
        <div className="all-products">
          {chooseLayout ? <Fragment>
              <div className="f f-end">
                <button className="filter-toggle" onClick={this.showFilters}>
                  {filterToggle ? <span>
                      <i className="fa fa-remove red-color" />
                      hide filters
                    </span> : <span>
                      <i className="fa fa-filter red-color" />
                      show filters
                    </span>}
                </button>
              </div>

              <div className="all-products-header" id="filterToggle" style={{ display: `${filterToggle ? "inline-block" : "none"}` }}>
                {accordion}
              </div>
            </Fragment> : <div className="all-products-sidebar">
              {accordion}
            </div>}
          {!hasLoaded ? <div className="flex relative" style={{ height: 300, width: "100%" }}>
              <Loading />
            </div> : <SearchResult result={result} resultMessage={resultMessage} itemsPerPage={itemsPerPage} resultLength={resultLength} hasLoaded={hasLoaded} handlePaginationChange={this.handlePaginationChange} focusSearchBar={this.focusSearchBar} />}
        </div>
        <Footer />
      </Fragment>;
  }
}

export default Search;
