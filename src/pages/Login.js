import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from "semantic-ui-react";
import auth from "../utils/AuthService";
import crud from "../utils/CRUDService";
import Logo from "../components/Single/Logo";
/* config */
import { BASE_URL } from "../config/host";

class Login extends Component {
  state = {
    email: "",
    password: "",
    emailRegisteredAlready: false,
    incorrectUsernameOrPassword: false,
    rememberMe: false,
    isAuthenticated: localStorage.getItem("isAuthenticated")
  };

  componentWillMount() {
    const { isAuthenticated } = this.state;
    const { history } = this.props;
    isAuthenticated && history.push("/");
  }

  handleInputChange = e => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  handleLogin = e => {
    e.preventDefault();
    // Get user input
    const { email, password } = this.state;
    const { history } = this.props;
    // Store input in payload
    const data = {
      email: email,
      password: password
    };
    // set loading to true
    this.setState({ isLoading: true });
    // Try Login up the user
    const cb = async response => {
      if (response.status === 200) {
        // Retreive username and data
        const {
          user: { username },
          token
        } = response.data.response.data;
        // Store isAuthenticated
        localStorage.setItem("isAuthenticated", true);
        // Set state isAuthenticated to true
        this.setState({ isAuthenticated: true });
        // Store token in localstorage
        auth.storeToken(token);
        localStorage.setItem("username", username);
        // Set Cart Count by getting cart and the length of the cart thereafter
        const cartURL = `${BASE_URL}cart/get/item/all`;
        const {
          data: {
            response: {
              data: { items }
            }
          }
        } = await crud.getWithToken(cartURL, token);
        let array = [];
        for (let i in items) {
          array.push(items[i]);
        }
        localStorage.setItem("cartCount", array.length);
        // Fetch the user details from the protected route using the token and store in localhost
        const customerDetails = await crud.getWithToken(
          `${BASE_URL}user/details`,
          token
        );
        if (customerDetails.status === 200) {
          localStorage.setItem(
            "customerDetails",
            JSON.stringify(customerDetails.data.response.data.user)
          );
        }
        // Send the user to home
        if (history.length === 2) {
          history.push("/");
        } else {
          history.goBack();
        }
      } else {
        // Do this when it got a response but response.status !== 200
        this.setState({ isLoading: false });
        console.log("An error occured along the way. We would find out why");
      }
    };
    auth.login(data, cb);
  };

  render() {
    const {
      isLoading,
      incorrectUsernameOrPassword,
      emailRegisteredAlready
    } = this.state;
    return (
      <div className="login-form">
        <style>
          {`
        body > div,
        body > div > div,
        body > div > div > div.login-form {
          height: 100%;
        }
      `}
        </style>
        <Grid
          textAlign="center"
          style={{ height: "100%" }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 375 }}>
            <Link to="/">
              <Header
                as="h1"
                color="grey"
                textAlign="center"
                className="authpage-header"
              >
                <Logo theme="white" />
              </Header>
            </Link>
            <Form size="large" onSubmit={this.handleLogin}>
              <Segment stacked>
                <Header as="h4" color="grey" textAlign="center">
                  Login to your account
                </Header>
                {emailRegisteredAlready && (
                  <Message
                    error
                    header="Email has already been registered"
                    content={
                      "You can only sign up for an account once with a given e-mail address."
                    }
                  />
                )}
                {incorrectUsernameOrPassword && (
                  <Message
                    error
                    header="Incorrect email or/and password"
                    content={
                      <div>
                        Check that you entered the correct credentials and try
                        again.<Link to="/forgot-password">
                          Forgot Password ?{" "}
                        </Link>
                      </div>
                    }
                  />
                )}

                <Form.Input
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder="Email address or username"
                  name="email"
                  type="text"
                  value={this.state.email}
                  onChange={this.handleInputChange}
                  required
                />
                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  type="password"
                  name="password"
                  value={this.state.password}
                  onChange={this.handleInputChange}
                  required
                />
                {/* <Form.Checkbox
                  label='Keep me signed in'
                  name="rememberMe"
                  type="checkbox"
                  checked={this.state.rememberMe}
                  onChange={this.handleRememberMe}
                /> */}
                <Button
                  fluid
                  size="mini"
                  loading={isLoading}
                  className="theme-bgcolor-2"
                >
                  Login
                </Button>
              </Segment>
            </Form>
            <Message>
              Don't have an account?{" "}
              <Link to="/signup" style={{ color: "red" }}>
                {" "}
                Sign up
              </Link>
            </Message>
          </Grid.Column>
        </Grid>
        <div className="bg-img">
          <span className="bg-overlay" />
          <img
            src="https://res.cloudinary.com/orinami/image/upload/f_auto,c_scale,q_25,w_1280/q_auto/v1522425318/bethany-newman-67011-unsplash_eydc7l.png"
            alt="homes.png"
          />
        </div>
      </div>
    );
  }
}

export default Login;
