import React, { Component } from "react";
import { Segment, Accordion, Form, Header } from "semantic-ui-react";
import PaystackButton from "react-paystack";
import { Link } from "react-router-dom";
import states from "../data/states.json";
import HeaderHOC from "../components/HOC/HeaderHOC";
import MobileFooter from "../components/Layout/Mobile/Footer";
import WebFooter from "../components/Layout/Web/Footer";
import LayoutHOC from "../components/HOC/Layout";
import notify from "../utils/notify";
import crud from "../utils/CRUDService";
/* config */
import { BASE_URL } from "../config/host";
/* urls */
const checkoutURL = `${BASE_URL}checkout`;
const cartURL = `${BASE_URL}cart/get/item/all`;
const token = localStorage.getItem("token");
const HeaderNav = HeaderHOC(false);
const Footer = LayoutHOC(MobileFooter, WebFooter);

class Checkout extends Component {
  state = {
    cart: [],
    key: "pk_test_67b61beb976eecb81333e26cc3068974682535b5",
    email: "xyz@example.com",
    amount: 100,
    billingEmailAddress: "",
    billingFirstName: "",
    billingLastName: "",
    billingAddressLineOne: "",
    billingAddressLineTwo: "",
    billingZipCode: "",
    billingCity: "",
    billingState: "",
    billingCountry: "",
    billingPhone: "",
    billingAltPhone: "",
    shippingEmailAddress: "",
    shippingFirstName: "",
    shippingLastName: "",
    shippingAddressLineOne: "",
    shippingAddressLineTwo: "",
    shippingZipCode: "",
    shippingCity: "",
    shippingState: "",
    shippingCountry: "",
    shippingPhone: "",
    shippingAltPhone: "",
    useAvailableToken: false,
    sameAsBillingAddress: false,
    isLoading: false
  };

  componentDidMount() {
    window.addEventListener("resize", this.handleWindowSizeChange);

    this._getCartItems();

    const customerDetails = JSON.parse(localStorage.getItem("customerDetails"));

    for (let i in customerDetails) {
      if (customerDetails[i] === null) {
        customerDetails[i] = "";
      }
    }

    customerDetails !== null &&
      this.setState({
        email: customerDetails.email,
        billingEmailAddress: customerDetails.email,
        billingFirstName: customerDetails.first_name,
        billingLastName: customerDetails.last_name,
        billingAddressLineOne: customerDetails.address1,
        billingAddressLineTwo: customerDetails.address2,
        billingZipCode: customerDetails.zipcode,
        billingCity: customerDetails.city,
        billingState: customerDetails.state,
        billingCountry: customerDetails.country,
        billingPhone: customerDetails.mobile_number,
        billingAltPhone: customerDetails.alternate_mobile_number
      });
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  _getCartItems = async () => {
    let cart = [];
    if (localStorage.getItem("token") !== null) {
      const {
        data: {
          response: {
            data: { items }
          }
        }
      } = await crud.getWithToken(cartURL, localStorage.getItem("token"));
      for (let i in items) {
        cart = [...cart, items[i]];
      }
    }
    this.setState({ cart });
  };

  callback = response => this._handleCheckout(response.reference);

  close = () => console.log("Payment closed");

  getReference = () => {
    //you can put any unique reference implementation code here
    let text = "";
    let possible =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < 30; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  };

  handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;

    this.setState({ activeIndex: newIndex });
  };

  handleInputChange = e => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  handleSelectChange = (e, { name, value }) => {
    this.setState({
      [name]: value
    });
  };
  onClickSameAsBillingAddress = () => {
    this.setState(prevState => ({
      sameAsBillingAddress: !prevState.sameAsBillingAddress
    }));
  };

  handleChangeSameAsBillingAddress = () => {
    this.setState({
      shippingEmailAddress: this.state.billingEmailAddress,
      shippingFirstName: this.state.billingFirstName,
      shippingLastName: this.state.billingLastName,
      shippingAddressLineOne: this.state.billingAddressLineOne,
      shippingAddressLineTwo: this.state.billingAddressLineTwo,
      shippingZipCode: this.state.billingZipCode,
      shippingCity: this.state.billingCity,
      shippingState: this.state.billingState,
      shippingCountry: this.state.billingCountry,
      shippingPhone: this.state.billingPhone,
      shippingAltPhone: this.state.billingAltPhone
    });
  };

  _handleCheckout = async reference => {
    const data = { reference };

    await crud.postWithToken(checkoutURL, data, token).then(response => {
      if (response.status === 200) {
        notify("success", "Your Order has been placed");
      } else {
        notify("error", "An error occur while placing your order, try again");
      }
    });

    // Get Checkout details
    // const {
    //   billingAddressLineOne,
    //   billingAddressLineTwo,
    //   billingAltPhone,
    //   billingCity,
    //   billingCountry,
    //   billingEmailAddress,
    //   billingFirstName,
    //   billingLastName,
    //   billingPhone,
    //   billingState,
    //   billingZipCode,
    //   shippingAddressLineOne,
    //   shippingAddressLineTwo,
    //   shippingAltPhone,
    //   sameAsBillingAddress,
    //   shippingCity,
    //   shippingCountry,
    //   shippingEmailAddress,
    //   shippingFirstName,
    //   shippingLastName,
    //   shippingPhone,
    //   shippingState,
    //   shippingZipCode
    //  } = this.state

    // Store input in payload
    // const data = {
    //   username: username,
    //   email: email,
    //   password: password,
    //   c_password: confirmPassword
    // }
    this.setState({ isLoading: true });
    // Try Signing up the user
    // try {
    //   const cb = async (response) => {
    //     if (response.status === 200) {
    //       // Retreive username and data
    //       const { success: { username, token } } = response.data
    //       // Store username and isAuthenticated
    //       localStorage.setItem('username', username);
    //       localStorage.setItem('isAuthenticated', true);
    //       // Store token in localstorage
    //       auth.storeToken(token)
    //       // Fetch the user details from the protected route using the token and store in localhost
    //       const customerDetails = await crud.getWithToken('https://homeauction-backend.herokuapp.com/api/user', token)
    //       console.log(customerDetails)
    //       if (customerDetails.status === 200) {
    //         localStorage.setItem('customerDetails', JSON.stringify(customerDetails.data))
    //       }
    //       // Set state isAuthenticated to true
    //       this.setState({ isAuthenticated: true })
    //       // Send the user to home
    //       history.push("/")
    //     } else {
    //       this.setState({ isLoading: false, errorOccured: true })
    //       console.log('An error occured along the way. We would find out why')
    //     }
    //   }
    //   auth.signup(data, cb)
    //   setTimeout(() => this.setState({ isLoading: false, errorOccured: true }), 5000)
    // }
    // catch (error) {
    //   this.setState({ isLoading: false })
    //   console.log(error)
    // }
  };

  render() {
    const cartCount = parseInt(localStorage.getItem("cartCount"), 10);
    const { activeIndex, sameAsBillingAddress, cart } = this.state;
    const { history } = this.props;
    let totalAmount = 0;
    for (let i in cart) {
      totalAmount += cart[i].product_id.product_unit_price * cart[i].quantity;
    }
    return (
      <div>
        <HeaderNav history={history} />
        <div className="cart-container">
          <h2>Checkout</h2>
          <Form onSubmit={this.handleCheckout}>
            <div className="billing">
              <Header as="h4" attached="top">
                Billing Address
              </Header>
              <Segment attached>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    label="Email address"
                    name="billingEmailAddress"
                    type="email"
                    value={this.state.billingEmailAddress}
                    onChange={this.handleInputChange}
                    required
                    autoFocus
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    label="First name"
                    name="billingFirstName"
                    type="text"
                    value={this.state.billingFirstName}
                    onChange={this.handleInputChange}
                    required
                  />
                  <Form.Input
                    fluid
                    label="Last name"
                    name="billingLastName"
                    type="text"
                    value={this.state.billingLastName}
                    onChange={this.handleInputChange}
                    required
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    label="Address Line 1"
                    name="billingAddressLineOne"
                    type="text"
                    value={this.state.billingAddressLineOne}
                    onChange={this.handleInputChange}
                    required
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    label="Address Line 2"
                    name="billingAddressLineTwo"
                    type="text"
                    value={this.state.billingAddressLineTwo}
                    onChange={this.handleInputChange}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  {/* <Form.Select fluid label='Country' options={options} placeholder='Gender' required/> */}
                  <Form.Input
                    fluid
                    label="Zip Code"
                    name="billingZipCode"
                    type="number"
                    value={this.state.billingZipCode}
                    onChange={this.handleInputChange}
                    required
                  />
                  <Form.Input
                    fluid
                    label="City"
                    name="billingCity"
                    type="text"
                    value={this.state.billingCity}
                    onChange={this.handleInputChange}
                    required
                  />

                  <Form.Select
                    fluid
                    label="State"
                    name="billingState"
                    options={states}
                    value={this.state.billingState}
                    onChange={this.handleSelectChange}
                    required
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    label="Phone"
                    name="billingPhone"
                    type="number"
                    value={this.state.billingPhone}
                    onChange={this.handleInputChange}
                    required
                  />
                  <Form.Input
                    fluid
                    label="Alt. Phone"
                    name="billingAltPhone"
                    type="number"
                    value={this.state.billingAltPhone}
                    onChange={this.handleInputChange}
                  />
                </Form.Group>
              </Segment>
            </div>
            <div className="shipping">
              <Header as="h4" attached="top">
                Shipping Address
              </Header>
              <Segment attached>
                <Form.Group widths="equal">
                  <Form.Checkbox
                    label="My shipping address is the same as billing address."
                    checked={sameAsBillingAddress}
                    onClick={this.onClickSameAsBillingAddress}
                    onChange={this.handleChangeSameAsBillingAddress}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    label="Email address"
                    name="shippingEmailAddress"
                    type="email"
                    value={
                      sameAsBillingAddress
                        ? this.state.shippingEmailAddress
                        : ""
                    }
                    onChange={this.handleInputChange}
                    required
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    label="First name"
                    name="shippingFirstName"
                    type="text"
                    value={
                      sameAsBillingAddress ? this.state.shippingFirstName : ""
                    }
                    onChange={this.handleInputChange}
                    required
                  />
                  <Form.Input
                    fluid
                    label="Last name"
                    name="shippingLastName"
                    type="text"
                    value={
                      sameAsBillingAddress ? this.state.shippingLastName : ""
                    }
                    onChange={this.handleInputChange}
                    required
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    label="Address Line 1"
                    name="shippingAddressLineOne"
                    type="text"
                    value={
                      sameAsBillingAddress
                        ? this.state.shippingAddressLineOne
                        : ""
                    }
                    onChange={this.handleInputChange}
                    required
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    label="Address Line 2"
                    name="shippingAddressLineTwo"
                    type="text"
                    value={
                      sameAsBillingAddress
                        ? this.state.shippingAddressLineTwo
                        : ""
                    }
                    onChange={this.handleInputChange}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  {/* <Form.Select fluid label='Country' options={options} placeholder='Gender' required/> */}
                  <Form.Input
                    fluid
                    label="Zip Code"
                    name="shippingZipCode"
                    type="number"
                    value={
                      sameAsBillingAddress ? this.state.shippingZipCode : ""
                    }
                    onChange={this.handleInputChange}
                    required
                  />
                  <Form.Input
                    fluid
                    label="City"
                    name="shippingCity"
                    type="text"
                    value={sameAsBillingAddress ? this.state.shippingCity : ""}
                    onChange={this.handleInputChange}
                    required
                  />

                  <Form.Select
                    fluid
                    label="State"
                    name="shippingState"
                    options={states}
                    value={sameAsBillingAddress ? this.state.shippingState : ""}
                    onChange={this.handleSelectChange}
                    required
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    label="Phone"
                    name="shippingPhone"
                    type="number"
                    value={sameAsBillingAddress ? this.state.shippingPhone : ""}
                    onChange={this.handleInputChange}
                    required
                  />
                  <Form.Input
                    fluid
                    label="Alt. Phone"
                    name="shippingAltPhone"
                    type="number"
                    value={
                      sameAsBillingAddress ? this.state.shippingAltPhone : ""
                    }
                    onChange={this.handleInputChange}
                  />
                </Form.Group>
              </Segment>
            </div>
            <div className="review-order">
              <Accordion fluid styled>
                <Accordion.Title
                  className="ui header active"
                  active={activeIndex === 0}
                  index={0}
                  onClick={this.handleClick}
                >
                  Review My Order
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 0}>
                  <div className="cart-main">
                    <header>
                      <span>Your Cart ({cartCount})</span>

                      <div className="mini-summary">
                        <div className="mini-summary-total">
                          <span>
                            Total:
                            <span className="mini-value">
                              <span>&#8358;</span>
                              {totalAmount}
                            </span>
                          </span>
                        </div>
                      </div>
                    </header>
                    <div className="cart-items">
                      {cart.length > 0 ? (
                        cart.map(item => (
                          <div
                            className="cart-item"
                            key={item.product_id.product_id}
                          >
                            <Link to={`/product/${item.product_id.product_id}`}>
                              <div className="cart-item-image">
                                <img
                                  src={item.product_id.product_image_url}
                                  alt="product"
                                />
                              </div>
                            </Link>
                            <div className="cart-details">
                              <div className="cart-details-meta">
                                <Link
                                  to={`/product/${item.product_id.product_id}`}
                                >
                                  <h3 className="cart-item-title">
                                    {item.product_name}
                                  </h3>
                                </Link>
                                <h4>
                                  Quantity: <span>{item.quantity}</span>
                                </h4>
                                <h4>
                                  SKU:{" "}
                                  <span>{item.product_id.product_sku}</span>
                                </h4>
                              </div>
                              {/* <div className="cart-details-action">
                                <button className="button" onClick={() => this.removeItem(item.product_id)}>Remove</button>
                                <button className="button" onClick={() => this.editItem(item.product_id)}>Edit</button>
                              </div> */}
                            </div>
                            <div className="cart-price">
                              <span>&#8358;</span>
                              {item.product_id.product_unit_price}
                            </div>
                          </div>
                        ))
                      ) : (
                        <div className="no-cart-item">
                          <h2>Your Cart is Empty</h2>
                          <Link to="/search?type=product">
                            <button className="button">Shop Now</button>
                          </Link>
                        </div>
                      )}
                    </div>
                  </div>
                </Accordion.Content>
              </Accordion>
            </div>
            <div className="checkout-payment">
              <Header as="h4" attached="top">
                Place Order
              </Header>
              <Segment attached className="checkout-payment-items">
                {/* <div className="checkout-use-token">
                  <Form.Checkbox label='Use Available Fremmiums' />
                </div> */}
                <div className="checkout-payment-gateway">
                  <img
                    src="https://res.cloudinary.com/orinami/raw/upload/v1517053691/cards_1_ejqaey.png"
                    alt=""
                  />
                </div>
              </Segment>
            </div>
            <div className="place-order-container">
              <PaystackButton
                text="Place Order"
                class="theme-button"
                callback={this.callback}
                close={this.close}
                reference={this.getReference()}
                email={this.state.email}
                amount={this.state.amount}
                paystackkey={this.state.key}
              />
            </div>
          </Form>
        </div>

        <Footer />
      </div>
    );
  }
}

export default Checkout;
