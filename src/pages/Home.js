import React, { Component, Fragment } from 'react';
import HeaderHOC from '../components/HOC/HeaderHOC';
import LayoutHOC from '../components/HOC/Layout';
import HomeForMobile from '../components/Home/Mobile/Body';
import HomeForWeb from '../components/Home/Web/Body';
import MobileFooter from '../components/Layout/Mobile/Footer';
import WebFooter from '../components/Layout/Web/Footer';

const Header = HeaderHOC(true);
const Main = LayoutHOC(HomeForMobile, HomeForWeb);
const Footer = LayoutHOC(MobileFooter, WebFooter);

class Home extends Component {
  render() {
    const { history } = this.props;
    return (
      <Fragment>
        <Header />
        <Main history={history} />
        <Footer />
      </Fragment>
    );
  }
}

export default Home;
