import React, { Component } from "react";
import { Link } from "react-router-dom";
/* components */
import MobileFooter from "../components/Layout/Mobile/Footer";
import WebFooter from "../components/Layout/Web/Footer";
/* HOCs */
import HeaderHOC from "../components/HOC/HeaderHOC";
import LayoutHOC from "../components/HOC/Layout";
/* utils */
import crud from "../utils/CRUDService";
import notify from "../utils/notify";
import scrollTop from "../utils/scroll";
/* config */
import { BASE_URL } from "../config/host";

const Header = HeaderHOC(false);
const Footer = LayoutHOC(MobileFooter, WebFooter);

const ww = window.outerWidth;
const cardWidth = 224;
const ic = Math.floor(ww / cardWidth) - 1;

const featuredURL = `${BASE_URL}featured/limit/${2 * ic}`;
const cartURL = `${BASE_URL}cart/get/item/all`;
const deleteCartItemURL = `${BASE_URL}cart/delete/item/`;
const updateCartURL = `${BASE_URL}cart/add/item`;
const token = localStorage.getItem("token");

class Cart extends Component {
  state = {
    cart: [],
    width: window.innerWidth,
    quantity: 1,
    total: 0,
    showUpdateButton: false,
    hasLoaded: false
  };

  componentDidMount() {
    window.addEventListener("resize", this.handleWindowSizeChange);
    this.getCartItems();
    scrollTop();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  getCartItems = async () => {
    let array = [];
    if (localStorage.getItem("token") !== null) {
      const {
        data: {
          response: {
            data: { items }
          }
        }
      } = await crud.getWithToken(cartURL, localStorage.getItem("token"));
      for (let i in items) {
        array.push(items[i]);
      }
    }
    this.setState({ cart: array });
  };

  handleRemoveItem = async cart_id => {
    await crud
      .getWithToken(`${deleteCartItemURL}${cart_id}`, token)
      .then(response => {
        if (response.status === 200) {
          const count = parseInt(localStorage.getItem("cartCount"), 10);
          localStorage.setItem("cartCount", count - 1);
          notify("success", "Item Removed Successfully");
        } else {
          notify("error", "Could not remove item");
        }
        console.log(response);
      });
    this.getCartItems();
  };

  handleUpdateItem = async product_id => {
    const { quantity } = this.state;
    console.log(quantity);
    const itemData = { product_id, quantity };
    await crud.postWithToken(updateCartURL, itemData, token).then(response => {
      if (response.status === 200) {
        notify("success", "Quantity updated");
        this.setState({ showUpdateButton: false });
      } else {
        notify("error", "Could not update quantity");
      }
      console.log(response);
    });
    this.getCartItems();
  };

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  handleInputChange = e => {
    this.setState({ quantity: e.target.value, showUpdateButton: true });
  };

  handleCalculateTotal = (price, quantity) => {
    const total = price * quantity;
    return total.toLocaleString("en");
  };

  render() {
    const { width, showUpdateButton, cart, featured, hasLoaded } = this.state;
    let sum = 0;
    const chooseLayout = width < 768;
    const { history } = this.props;
    let cartCount;
    cart === [] ? (cartCount = 0) : (cartCount = cart.length);

    for (let i in cart) {
      sum += cart[i].product_id.product_unit_price * cart[i].quantity;
    }
    return (
      <div>
        <Header history={history} />
        <div className="cart-container">
          <div className="cart-navigation">
            <div className="continue-shopping">
              <Link to="/search?type=product">
                <span>
                  <i className="fa fa-arrow-left" />
                </span>
                Continue shopping
              </Link>
            </div>
          </div>
          <div className="cart-self">
            <div className="cart-main">
              <header>
                <span>Your Cart ({cartCount})</span>
                {chooseLayout && (
                  <div className="mini-summary">
                    <div className="mini-summary-total">
                      <span>
                        Total:
                        <span className="mini-value">
                          <span>&#8358;</span>
                          {sum}
                        </span>
                      </span>
                    </div>
                    <Link to="/checkout" className="mini-checkout">
                      Checkout
                      <span>
                        <i className="fa fa-arrow-right" />
                      </span>
                    </Link>
                  </div>
                )}
              </header>
              <div className="cart-items">
                {cart.length > 0 ? (
                  cart.map(item => (
                    <div className="cart-item" key={item.product_id.product_id}>
                      <Link to={`/product/${item.product_id.product_id}`}>
                        <div className="cart-item-image">
                          <img
                            src={item.product_id.product_image_url}
                            alt="product"
                          />
                        </div>
                      </Link>
                      <div className="cart-details">
                        <div className="cart-details-meta">
                          <Link to={`/product/${item.product_id.product_id}`}>
                            <h3 className="cart-item-title">
                              {item.product_id.product_name}
                            </h3>
                          </Link>
                          <h4>
                            Unit Price:{" "}
                            <span>
                              {item.product_id.product_unit_price.toLocaleString(
                                "en"
                              )}
                            </span>
                          </h4>
                          <h4>
                            Quantity:
                            <span>
                              <input
                                type="number"
                                min={1}
                                max={1000}
                                id="cQuantity"
                                defaultValue={item.quantity}
                                name="quantitiy"
                                onChange={this.handleInputChange}
                              />
                            </span>
                          </h4>
                        </div>
                        <div className="cart-details-action">
                          {showUpdateButton && (
                            <button
                              className="button"
                              onClick={() =>
                                this.handleUpdateItem(
                                  item.product_id.product_id
                                )
                              }
                            >
                              Update
                            </button>
                          )}
                          <button
                            className="button"
                            onClick={() => this.handleRemoveItem(item.cart_id)}
                          >
                            Remove
                          </button>
                        </div>
                      </div>
                      <div className="cart-price">
                        <span>&#8358;</span>
                        {this.handleCalculateTotal(
                          item.product_id.product_unit_price,
                          item.quantity
                        )}
                      </div>
                    </div>
                  ))
                ) : (
                  <div className="no-cart-item">
                    <h4>Your Cart is Empty</h4>
                    <Link to="/search?type=product">
                      <button className="button">Shop Now</button>
                    </Link>
                  </div>
                )}
              </div>
            </div>
            <div
              className="cart-summary-container"
              style={chooseLayout ? { display: "none" } : {}}
            >
              <div className="cart-summary">
                <header>Summary</header>
                <div className="cart-summary-details">
                  {/* <h3 className="cart-summary-total">Use Token: {total}</h3> */}
                  <h3 className="cart-summary-total">
                    Total: <span>&#8358;</span>
                    {sum.toLocaleString("en")}
                  </h3>
                  {cart.length > 0 && (
                    <Link to="/checkout">
                      <button className="theme-button">
                        Continue to Checkout
                      </button>
                    </Link>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Cart;
