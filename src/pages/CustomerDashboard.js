import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import { Icon } from "semantic-ui-react";
// utils
// import Auth from '../utils/AuthService'
import Capitalize from "../utils/Capitalize";
// config
import * as RouteHandler from "../config/RoutesHandler.js";
// import app components
import Logo from "../components/Single/Logo";
import Home from "../components/CustomerDashboard/Home";
import Account from "../components/CustomerDashboard/Account";
import Order from "../components/CustomerDashboard/Order";
import ManageAuction from "../components/CustomerDashboard/ManageAuction";
import ManageProduct from "../components/CustomerDashboard/ManageProduct";
import Bid from "../components/CustomerDashboard/Bid";
import Wallet from "../components/CustomerDashboard/Wallet";
import Reward from "../components/CustomerDashboard/Reward";

const routes = [
  {
    path: "/dashboard",
    exact: true,
    sidebar: () => <div />,
    main: () => <Home />
  },
  {
    path: "/dashboard/account",
    exact: true,
    sidebar: () => <div />,
    main: () => <Account />
  },
  {
    path: "/dashboard/orders",
    exact: true,
    sidebar: () => <div />,
    main: () => <Order />
  },
  {
    path: "/dashboard/manage-auctions",
    exact: true,
    sidebar: () => <div />,
    main: () => <ManageAuction />
  },
  {
    path: "/dashboard/manage-products",
    exact: true,
    sidebar: () => <div />,
    main: () => <ManageProduct />
  },
  {
    path: "/dashboard/bids",
    exact: true,
    sidebar: () => <div />,
    main: () => <Bid />
  },
  {
    path: "/dashboard/wallet",
    exact: true,
    sidebar: () => <div />,
    main: () => <Wallet />
  },
  {
    path: "/dashboard/rewards",
    exact: true,
    sidebar: () => <div />,
    main: () => <Reward />
  }
];

const li = [
  {
    link: "/dashboard",
    name: "Home",
    icon: "home"
  },
  {
    link: "/dashboard/account",
    name: "My Profile",
    icon: "user"
  },
  {
    link: "/dashboard/orders",
    name: "My Orders",
    icon: "cubes"
  },
  {
    link: "/dashboard/manage-auctions",
    name: "Manage Auctions",
    icon: "sitemap"
  },
  {
    link: "/dashboard/manage-products",
    name: "Manage Products",
    icon: "sitemap"
  },
  {
    link: "/dashboard/bids",
    name: "My Bids",
    icon: "hand-paper-o"
  },
  {
    link: "/dashboard/wallet",
    name: "My Wallet",
    icon: "credit-card-alt"
  },
  {
    link: "/dashboard/rewards",
    name: "My Rewards",
    icon: "trophy"
  }
];

class CustomerDashboard extends Component {
  state = {
    isAuthenticated: localStorage.getItem("isAuthenticated"),
    activeItem: "bio",
    username: localStorage.getItem("username"),
    width: window.innerWidth
  };

  componentWillMount() {
    !this.state.isAuthenticated && this.props.history.push("/login");
  }

  componentDidMount() {
    window.addEventListener("resize", this.handleWindowSizeChange);
    const customerDetails = JSON.parse(localStorage.getItem("customerDetails"));
    if (customerDetails !== null && this.state.username === null) {
      this.setState({
        username: customerDetails.username
      });
    }

    const sidebar = document.getElementById("m-sidebar");
    const menu = document.getElementById("menu");

    if (menu !== null) menu.onclick = () => (sidebar.style.display = "flex");

    window.onclick = e => {
      if (e.target === sidebar) {
        sidebar.style.display = "none";
      }
    };
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => this.setState({ width: window.innerWidth });

  onClickHome = () => this.props.history.push("/");

  render() {
    const { username, width } = this.state;
    const chooseLayout = width <= 1024;

    const menubar = (
      <div className="cd-header-items">
        <div className="site-menu" id="menu">
          <i className="fa fa-bars fa-2x cd-menu" />
        </div>
        <div className="m-sidenav" id="m-sidebar">
          <div className="m-nav-items">
            <ul>
              <li className="m-nav-item">
                <Link to="/dashboard">Home</Link>
              </li>
              <li className="m-nav-item">
                <Link to="/dashboard/account">My Profile</Link>
              </li>
              <li className="m-nav-item">
                <Link to="/dashboard/orders">My Orders</Link>
              </li>
              <li className="m-nav-item">
                <Link to="/dashboard/manage-auctions">Manage Auctions</Link>
              </li>
              <li className="m-nav-item">
                <Link to="/dashboard/manage-products">Manage Products</Link>
              </li>
              <li className="m-nav-item">
                <Link to="/dashboard/bids">My bids</Link>
              </li>
              <li className="m-nav-item">
                <Link to="/dashboard/wallet">My Wallet</Link>
              </li>
              <li className="m-nav-item">
                <Link to="/dashboard/rewards">My Rewards</Link>
              </li>
              <li className="m-nav-item red-color">
                <Link to="/logout">Logout</Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );

    const displayName = (
      <div className="cd-header-items">
        <Icon name="user" /> Hello, {username !== null && Capitalize(username)}
        <Link to="/logout" className="dashboard-logout">
          Logout
        </Link>
      </div>
    );

    return (
      <div className="customer-dashboard">
        <div className="cd-header">
          <div className="cd-header-logo">
            <Link to="/">
              <Logo theme="white" />
            </Link>
          </div>
          {chooseLayout ? menubar : displayName}
        </div>
        <div className="cd-container">
          {!chooseLayout && (
            <div className="cd-sidebar">
              <ul style={{ position: "sticky", top: "64px" }}>
                {li.map((li, index) => (
                  <Link to={li.link} key={index}>
                    <li className="cd-sidebar-item">
                      <span className="cd-sb-icon">
                        <i className={`fa fa-${li.icon}`} />
                      </span>
                      <span className="cd-sb-name">{li.name}</span>
                      <span className="cd-sb-caret-right">
                        <i className="fa fa-angle-right" />
                      </span>
                    </li>
                  </Link>
                ))}
              </ul>
            </div>
          )}
          <div className="cd-main-container">
            {routes.map((route, index) => (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.main}
              />
            ))}
          </div>
        </div>
        <Route exact path="/" component={RouteHandler.Home} />
        <Route path="/auction/:id" component={RouteHandler.Auction} />
        <Route path="/product/:id" component={RouteHandler.Product} />
        <Route exact path="/logout" component={RouteHandler.Logout} />
      </div>
    );
  }
}

export default CustomerDashboard;
