import Noty from 'noty';

export default function Notify(type, text) {
  return new Noty({
    type: type,
    layout: 'topRight',
    timeout: 3000,
    text: text
  }).show();
}
