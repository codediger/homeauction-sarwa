function scrollUp() {
  var yscroll = -window.scrollY;
  var scrollStep = yscroll / (500 / 15);
  var scrollInterval = setInterval(function() {
    window.scrollY !== 0
      ? window.scrollBy(0, scrollStep)
      : clearInterval(scrollInterval);
  }, 15);
}

export default scrollUp;
