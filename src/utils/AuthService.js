import { post } from 'axios';

/* config */

import { BASE_URL } from '../config/host';

const AuthService = {
  /**
   * @param {Object} login - Code to handle login
   */
  login: (data, cb) => {
    try {
      post(`${BASE_URL}login`, { ...data })
        .then(response => {
          cb(response);
        })
        .catch(error => console.log(error));
    } catch (error) {
      console.log('Login error');
      console.error(new Error(error));
    }
  },

  /**
   * @param {Object} signup - Code to handle signup
   */
  signup: (data, cb) => {
    try {
      post(`${BASE_URL}register`, { ...data })
        .then(response => {
          cb(response);
        })
        .catch(error => console.log(error));
    } catch (error) {
      console.log('Signup error');
      console.error(new Error(error));
    }
  },

  /**
   * @param {} logout - Code to handle when a user logs out
   */

  logout: history => {
    localStorage.clear();
    history.push('/'); // redirect to home
  },

  // /**
  //   * @param {} isAuthenticated - Check if a user is authenticated
  //   */

  isAuthenticated: () => {
    return localStorage.getItem('isAuthenticated');
  },

  /**
   * @param {String} storeToken - Store the auth token in the local storage of the web browser
   */

  storeToken: token => {
    try {
      localStorage.setItem('token', token);
    } catch (error) {
      console.log('Failed to store auth credentials');
      console.error(new Error(error));
    }
  }
};

export default AuthService;
